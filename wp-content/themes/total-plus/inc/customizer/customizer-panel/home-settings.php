<?php

$customizer_home_settings = of_get_option('customizer_home_settings', '1');
if (!$customizer_home_settings) {
    return;
}

/**
 * Total Plus Theme Customizer
 *
 * @package Total Plus
 */
/* HOME PANEL */
$wp_customize->add_panel('total_plus_home_panel', array(
    'title' => esc_html__('Home Sections/Settings', 'total-plus'),
    'description' => esc_html__('Drag and Drop to Reorder', 'total-plus') . '<img class="total-drag-spinner" src="' . admin_url('/images/spinner.gif') . '">',
    'priority' => 20
));

/* ============SLIDER IMAGES SECTION============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_slider_section', array(
    'title' => esc_html__('Home Slider', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => -1,
    'hiding_control' => 'total_plus_slider_disable',
)));

//ENABLE/DISABLE SLIDER
$wp_customize->add_setting('total_plus_slider_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_slider_disable', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_slider_type', array(
    'default' => 'normal',
    'sanitize_callback' => 'total_plus_sanitize_choices'
));

$wp_customize->add_control('total_plus_slider_type', array(
    'section' => 'total_plus_slider_section',
    'type' => 'radio',
    'label' => esc_html__('Slider Type', 'total-plus'),
    'choices' => array(
        'normal' => esc_html__('Normal Slider', 'total-plus'),
        'revolution' => esc_html__('Revolution Slider', 'total-plus'),
        'banner' => esc_html__('Single Banner Image', 'total-plus')
    )
));

/* Slider */
$wp_customize->add_setting('total_plus_slider_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_slider_heading', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Sliders', 'total-plus')
)));

$wp_customize->add_setting('total_plus_sliders', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'image' => '',
            'title' => '',
            'subtitle' => '',
            'button_link' => '',
            'button_text' => esc_html__('Read More', 'total-plus'),
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_sliders', array(
    'label' => esc_html__('Add Sliders', 'total-plus'),
    'section' => 'total_plus_slider_section',
    'box_label' => esc_html__('Slider', 'total-plus'),
    'add_label' => esc_html__('Add Slider', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
        ), array(
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Image', 'total-plus'),
        'default' => ''
    ),
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Slider Caption Title', 'total-plus'),
        'default' => ''
    ),
    'subtitle' => array(
        'type' => 'textarea',
        'label' => esc_html__('Slider Caption Subtitle', 'total-plus'),
        'default' => ''
    ),
    'button_link' => array(
        'type' => 'text',
        'label' => esc_html__('Slider Button Link', 'total-plus'),
        'default' => ''
    ),
    'button_text' => array(
        'type' => 'text',
        'label' => esc_html__('Slider Button Text', 'total-plus'),
        'default' => esc_html__('Read More', 'total-plus')
    ),
    'alignment' => array(
        'type' => 'select',
        'label' => esc_html__('Slider Caption Alignment', 'total-plus'),
        'default' => 'left',
        'options' => array(
            'center' => esc_html__('Center', 'total-plus'),
            'left' => esc_html__('Left', 'total-plus'),
            'right' => esc_html__('Right', 'total-plus')
        )
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Slider', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_slider_info', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Info_Text($wp_customize, 'total_plus_slider_info', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Note:', 'total-plus'),
    'description' => esc_html__('Recommended Image Size: 1900X800', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_slider_setting_heading', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Slider Settings', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_overlay_color', array(
    'default' => 'rgba(255,255,255,0)',
    'sanitize_callback' => 'total_plus_sanitize_color_alpha',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Alpha_Color_Control($wp_customize, 'total_plus_slider_overlay_color', array(
    'label' => esc_html__('Slider Overlay Color', 'total-plus'),
    'section' => 'total_plus_slider_section',
    'palette' => array(
        'rgb(255, 255, 255, 0.3)', // RGB, RGBa, and hex values supported
        'rgba(0, 0, 0, 0.3)',
        'rgba( 255, 255, 255, 0.2 )', // Different spacing = no problem
        '#00CC99', // Mix of color types = no problem
        '#00C439',
        '#00C569',
        'rgba( 255, 234, 255, 0.2 )', // Different spacing = no problem
        '#AACC99', // Mix of color types = no problem
        '#33C439',
    ),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_pause', array(
    'default' => '5',
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_slider_pause', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Slider Pause Duration', 'total-plus'),
    'description' => esc_html__('Slider Pause duration in seconds', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 10,
        'step' => 1,
    ),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_full_screen', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => false,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_slider_full_screen', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Full Screen Slider', 'total-plus'),
    'description' => esc_html__('Image may crop on either sides to cover the full screen.', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_arrow', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_slider_arrow', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Slider Arrow', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));

$wp_customize->add_setting('total_plus_slider_dots', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => false,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_slider_dots', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Slider Dots', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_normal'
)));


/* Banner */
$wp_customize->add_setting('total_plus_banner_image', array(
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'total_plus_banner_image', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Upload Banner Image', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_banner',
)));

$wp_customize->add_setting('total_plus_banner_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_title', array(
    'section' => 'total_plus_slider_section',
    'type' => 'text',
    'label' => esc_html__('Banner Title', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_subtitle', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_subtitle', array(
    'section' => 'total_plus_slider_section',
    'type' => 'textarea',
    'label' => esc_html__('Banner SubTitle', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_button_text', array(
    'section' => 'total_plus_slider_section',
    'type' => 'text',
    'label' => esc_html__('Button Text', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_button_link', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_button_link', array(
    'section' => 'total_plus_slider_section',
    'type' => 'text',
    'label' => esc_html__('Banner Link', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_text_alignment', array(
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'default' => 'center',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_text_alignment', array(
    'type' => 'select',
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Banner Text Alignment', 'total-plus'),
    'choices' => array(
        'left' => esc_html__('Left', 'total-plus'),
        'right' => esc_html__('Right', 'total-plus'),
        'center' => esc_html__('Center', 'total-plus')
    ),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_parallax_effect', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'none',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_banner_parallax_effect', array(
    'type' => 'radio',
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Background Effect', 'total-plus'),
    'choices' => array(
        'none' => esc_html__('None', 'total-plus'),
        'parallax' => esc_html__('Enable Parallax', 'total-plus'),
        'scroll' => esc_html__('Horizontal Moving', 'total-plus')
    ),
    'active_callback' => 'total_plus_check_slider_type_banner'
));

$wp_customize->add_setting('total_plus_banner_overlay_color', array(
    'default' => 'rgba(255,255,255,0)',
    'sanitize_callback' => 'total_plus_sanitize_color_alpha',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Alpha_Color_Control($wp_customize, 'total_plus_banner_overlay_color', array(
    'label' => esc_html__('Background Overlay Color', 'total-plus'),
    'section' => 'total_plus_slider_section',
    'palette' => array(
        'rgb(255, 255, 255, 0.3)', // RGB, RGBa, and hex values supported
        'rgba(0, 0, 0, 0.3)',
        'rgba( 255, 255, 255, 0.2 )', // Different spacing = no problem
        '#00CC99', // Mix of color types = no problem
        '#00C439',
        '#00C569',
        'rgba( 255, 234, 255, 0.2 )', // Different spacing = no problem
        '#AACC99', // Mix of color types = no problem
        '#33C439',
    ),
    'active_callback' => 'total_plus_check_slider_type_banner'
)));

/* Revolution Slider Shortcode */
$wp_customize->add_setting('total_plus_slider_shortcode', array(
    'default' => '',
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control('total_plus_slider_shortcode', array(
    'section' => 'total_plus_slider_section',
    'type' => 'text',
    'label' => esc_html__('Slider ShortCode', 'total-plus'),
    'description' => esc_html__('Add the ShortCode for Slider', 'total-plus'),
    'active_callback' => 'total_plus_check_slider_type_rev'
));

$wp_customize->add_setting('total_plus_slider_seperator2', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Separator_Control($wp_customize, 'total_plus_slider_seperator2', array(
    'section' => 'total_plus_slider_section'
)));

$wp_customize->add_setting('total_plus_slider_bottom_seperator', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'none',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_slider_bottom_seperator', array(
    'section' => 'total_plus_slider_section',
    'type' => 'select',
    'label' => esc_html__('Bottom Seperator', 'total-plus'),
    'choices' => array_merge(array('none' => 'None'), total_plus_svg_seperator())
));

$wp_customize->add_setting('total_plus_slider_bs_color', array(
    'default' => '#FF0000',
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_slider_bs_color', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Bottom Seperator Color', 'total-plus')
)));

$wp_customize->add_setting('total_plus_slider_bs_height', array(
    'sanitize_callback' => 'absint',
    'default' => 60,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_slider_bs_height', array(
    'section' => 'total_plus_slider_section',
    'label' => esc_html__('Bottom Seperator Height (px)', 'total-plus'),
    'options' => array(
        'min' => 20,
        'max' => 200,
        'step' => 1,
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_sliders', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_slider_pause', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_slider_full_screen', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_slider_arrow', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_slider_dots', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_slider_bottom_seperator', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_banner_button_link', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_banner_parallax_effect', array(
    'selector' => '#ht-home-slider-section',
    'render_callback' => 'total_plus_slider_section',
    'container_inclusive' => true
));

/* ============ABOUT US SECTION============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_about_section', array(
    'title' => esc_html__('About Us Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_about_section'),
    'hiding_control' => 'total_plus_about_page_disable'
)));


$wp_customize->add_setting('total_plus_about_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_about_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_about_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_about_page_disable',
                'total_plus_about_page_heading',
                'total_plus_about_page',
                'total_plus_progressbar',
                'total_plus_about_sidebar_heading',
                'total_plus_disable_about_sidebar',
                'total_plus_about_image',
                'total_plus_about_widget',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_about_enable_fullwindow',
                'total_plus_about_align_item',
                'total_plus_about_bg_type',
                'total_plus_about_bg_color',
                'total_plus_about_bg_gradient',
                'total_plus_about_bg_image',
                'total_plus_about_parallax_effect',
                'total_plus_about_bg_video',
                'total_plus_about_overlay_color',
                'total_plus_about_cs_heading',
                'total_plus_about_super_title_color',
                'total_plus_about_title_color',
                'total_plus_about_text_color',
                'total_plus_about_link_color',
                'total_plus_about_link_hov_color',
                'total_plus_about_cs_seperator',
                'total_plus_about_padding_top',
                'total_plus_about_padding_bottom',
                'total_plus_about_section_seperator',
                'total_plus_about_seperator1',
                'total_plus_about_top_seperator',
                'total_plus_about_ts_color',
                'total_plus_about_ts_height',
                'total_plus_about_seperator2',
                'total_plus_about_bottom_seperator',
                'total_plus_about_bs_color',
                'total_plus_about_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE ABOUT US PAGE
$wp_customize->add_setting('total_plus_about_page_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_about_page_disable', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_about_page_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_about_page_heading', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('About Page - Left Block', 'total-plus')
)));

//ABOUT US PAGE
$wp_customize->add_setting('total_plus_about_page', array(
    'sanitize_callback' => 'absint',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_about_page', array(
    'section' => 'total_plus_about_section',
    'type' => 'dropdown-pages',
    'label' => esc_html__('Select a Page', 'total-plus')
));


$wp_customize->add_setting('total_plus_progressbar', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'title' => '',
            'percentage' => 50,
            'enable' => 'on'
        )
    ))
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_progressbar', array(
    'label' => esc_html__('Progress Bars', 'total-plus'),
    'section' => 'total_plus_about_section',
    'box_label' => esc_html__('Progress Bar', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'percentage' => array(
        'type' => 'range',
        'label' => esc_html__('Precentage', 'total-plus'),
        'options' => array(
            'min' => 0,
            'max' => 100,
            'step' => 1,
            'unit' => '%',
            'val' => '50'
        ),
        'default' => '50'
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_about_sidebar_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_about_sidebar_heading', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('Sidebar - Right Block', 'total-plus')
)));

$wp_customize->add_setting('total_plus_disable_about_sidebar', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_disable_about_sidebar', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('Disable Sidebar', 'total-plus'),
    'description' => esc_html__('If disabled, the left content will cover the full width', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    )
)));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_about_image_heading', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('Right Image', 'total-plus')
)));

$wp_customize->add_setting('total_plus_about_image', array(
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'total_plus_about_image', array(
    'section' => 'total_plus_about_section',
    'label' => esc_html__('Upload Image', 'total-plus'),
    'description' => esc_html__('Recommended Image Size: 500X600px', 'total-plus')
)));

$wp_customize->add_setting('total_plus_about_widget', array(
    'default' => '0',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_about_widget', array(
    'section' => 'total_plus_about_section',
    'type' => 'select',
    'label' => esc_html__('Replace Image by widget', 'total-plus'),
    'choices' => $total_plus_widget_list
));

$wp_customize->selective_refresh->add_partial('total_plus_about_page', array(
    'selector' => '.ht-about-page',
    'render_callback' => 'total_plus_about_page'
));

$wp_customize->selective_refresh->add_partial('total_plus_progressbar', array(
    'selector' => '.ht-progressbar-container',
    'render_callback' => 'total_plus_about_progressbar'
));

$wp_customize->selective_refresh->add_partial('total_plus_disable_about_sidebar', array(
    'selector' => '.ht-about-container',
    'render_callback' => 'total_plus_about_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_about_image', array(
    'selector' => '.ht-about-sidebar',
    'render_callback' => 'total_plus_about_sidebar',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_about_widget', array(
    'selector' => '.ht-about-sidebar',
    'render_callback' => 'total_plus_about_sidebar',
    'container_inclusive' => true
));


/* ============FEATURED SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_featured_section', array(
    'title' => esc_html__('Featured Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_featured_section'),
    'hiding_control' => 'total_plus_featured_section_disable'
)));

$wp_customize->add_setting('total_plus_featured_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_featured_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_featured_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_featured_section_disable',
                'total_plus_featured_title_sub_title_heading',
                'total_plus_featured_super_title',
                'total_plus_featured_title',
                'total_plus_featured_sub_title',
                'total_plus_featured_title_style',
                'total_plus_featured_block_heading',
                'total_plus_featured',
                'total_plus_featured_button_text',
                'total_plus_featured_button_link',
                'total_plus_featured_setting_heading',
                'total_plus_featured_style',
                'total_plus_featured_col'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_featured_enable_fullwindow',
                'total_plus_featured_align_item',
                'total_plus_featured_bg_type',
                'total_plus_featured_bg_color',
                'total_plus_featured_bg_gradient',
                'total_plus_featured_bg_image',
                'total_plus_featured_parallax_effect',
                'total_plus_featured_bg_video',
                'total_plus_featured_overlay_color',
                'total_plus_featured_cs_heading',
                'total_plus_featured_super_title_color',
                'total_plus_featured_title_color',
                'total_plus_featured_text_color',
                'total_plus_featured_link_color',
                'total_plus_featured_link_hov_color',
                'total_plus_featured_mb_bg_color',
                'total_plus_featured_mb_text_color',
                'total_plus_featured_mb_hov_bg_color',
                'total_plus_featured_mb_hov_text_color',
                'total_plus_featured_cs_seperator',
                'total_plus_featured_padding_top',
                'total_plus_featured_padding_bottom',
                'total_plus_featured_section_seperator',
                'total_plus_featured_seperator1',
                'total_plus_featured_top_seperator',
                'total_plus_featured_ts_color',
                'total_plus_featured_ts_height',
                'total_plus_featured_seperator2',
                'total_plus_featured_bottom_seperator',
                'total_plus_featured_bs_color',
                'total_plus_featured_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE FEATURED SECTION
$wp_customize->add_setting('total_plus_featured_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_featured_section_disable', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus'),
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_featured_title_sub_title_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_featured_title_sub_title_heading', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_featured_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_super_title', array(
    'section' => 'total_plus_featured_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_featured_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Featured Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_title', array(
    'section' => 'total_plus_featured_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_featured_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Featured Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_sub_title', array(
    'section' => 'total_plus_featured_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_featured_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_title_style', array(
    'section' => 'total_plus_featured_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

//FEATURED BLOCK

$wp_customize->add_setting('total_plus_featured_block_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_featured_block_heading', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('Featured Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_featured', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'icon' => 'icofont-angry-monster',
            'title' => '',
            'content' => '',
            'link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_featured', array(
    //'label'   => esc_html__('Highlight Page','total-plus'),
    'section' => 'total_plus_featured_section',
    'box_label' => esc_html__('Featured Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'icon' => array(
        'type' => 'icon',
        'label' => esc_html__('Select Icon', 'total-plus'),
        'default' => 'icofont-angry-monster'
    ),
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Content', 'total-plus'),
        'default' => ''
    ),
    'link' => array(
        'type' => 'text',
        'label' => esc_html__('Link', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_featured_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_button_text', array(
    'section' => 'total_plus_featured_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_featured_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_featured_button_link', array(
    'section' => 'total_plus_featured_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_featured_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_featured_setting_heading', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_featured_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_featured_style', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('Featured Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/featured-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/featured-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/featured-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/featured-style4.png',
        'style5' => $imagepath . '/inc/customizer/images/featured-style5.png',
        'style6' => $imagepath . '/inc/customizer/images/featured-style6.png',
        'style7' => $imagepath . '/inc/customizer/images/featured-style7.png',
    )
)));

$wp_customize->add_setting('total_plus_featured_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_featured_col', array(
    'section' => 'total_plus_featured_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 6,
        'step' => 1,
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_featured_title_style', array(
    'selector' => '.ht-featured-section',
    'render_callback' => 'total_plus_featured_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_super_title', array(
    'selector' => '.ht-featured-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_featured_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_title', array(
    'selector' => '.ht-featured-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_featured_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_sub_title', array(
    'selector' => '.ht-featured-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_featured_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_featured', array(
    'selector' => '.ht-featured-content',
    'render_callback' => 'total_plus_featured_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_style', array(
    'selector' => '.ht-featured-content',
    'render_callback' => 'total_plus_featured_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_col', array(
    'selector' => '.ht-featured-content',
    'render_callback' => 'total_plus_featured_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_button_text', array(
    'selector' => '.ht-featured-section',
    'render_callback' => 'total_plus_featured_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_featured_button_link', array(
    'selector' => '.ht-featured-section',
    'render_callback' => 'total_plus_featured_section',
    'container_inclusive' => true
));

/* ============HIGHLIGHT SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_highlight_section', array(
    'title' => esc_html__('Highlight Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_highlight_section'),
    'hiding_control' => 'total_plus_highlight_section_disable'
)));

$wp_customize->add_setting('total_plus_highlight_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_highlight_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_highlight_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_highlight_section_disable',
                'total_plus_highlight_title_sub_title_heading',
                'total_plus_highlight_super_title',
                'total_plus_highlight_title',
                'total_plus_highlight_sub_title',
                'total_plus_highlight_title_style',
                'total_plus_highlight_block_heading',
                'total_plus_highlight',
                'total_plus_highlight_button_text',
                'total_plus_highlight_button_link',
                'total_plus_highlight_setting_heading',
                'total_plus_highlight_style',
                'total_plus_highlight_col'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_highlight_enable_fullwindow',
                'total_plus_highlight_align_item',
                'total_plus_highlight_bg_type',
                'total_plus_highlight_bg_color',
                'total_plus_highlight_bg_gradient',
                'total_plus_highlight_bg_image',
                'total_plus_highlight_parallax_effect',
                'total_plus_highlight_bg_video',
                'total_plus_highlight_overlay_color',
                'total_plus_highlight_cs_heading',
                'total_plus_highlight_super_title_color',
                'total_plus_highlight_title_color',
                'total_plus_highlight_text_color',
                'total_plus_highlight_link_color',
                'total_plus_highlight_link_hov_color',
                'total_plus_highlight_mb_bg_color',
                'total_plus_highlight_mb_text_color',
                'total_plus_highlight_mb_hov_bg_color',
                'total_plus_highlight_mb_hov_text_color',
                'total_plus_highlight_cs_seperator',
                'total_plus_highlight_padding_top',
                'total_plus_highlight_padding_bottom',
                'total_plus_highlight_section_seperator',
                'total_plus_highlight_seperator1',
                'total_plus_highlight_top_seperator',
                'total_plus_highlight_ts_color',
                'total_plus_highlight_ts_height',
                'total_plus_highlight_seperator2',
                'total_plus_highlight_bottom_seperator',
                'total_plus_highlight_bs_color',
                'total_plus_highlight_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE FEATURED SECTION
$wp_customize->add_setting('total_plus_highlight_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_highlight_section_disable', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_highlight_title_sub_title_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_highlight_title_sub_title_heading', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_highlight_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_super_title', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_highlight_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Highlight Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_title', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_highlight_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Highlight Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_sub_title', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_highlight_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_title_style', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

//HIGHLIGHT BLOCK

$wp_customize->add_setting('total_plus_highlight_block_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_highlight_block_heading', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('Highlight Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_highlight', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'icon' => 'icofont-angry-monster',
            'image' => '',
            'title' => '',
            'content' => '',
            'link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_highlight', array(
    'section' => 'total_plus_highlight_section',
    'box_label' => esc_html__('Highlight Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'icon' => array(
        'type' => 'icon',
        'label' => esc_html__('Select Icon', 'total-plus'),
        'default' => 'icofont-angry-monster'
    ),
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Image', 'total-plus'),
        'default' => ''
    ),
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Content', 'total-plus'),
        'default' => ''
    ),
    'link' => array(
        'type' => 'text',
        'label' => esc_html__('Link', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_highlight_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_button_text', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_highlight_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_highlight_button_link', array(
    'section' => 'total_plus_highlight_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_highlight_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_highlight_setting_heading', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_highlight_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_highlight_style', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('Highlight Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/highlight-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/highlight-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/highlight-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/highlight-style4.png'
    )
)));

$wp_customize->add_setting('total_plus_highlight_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_highlight_col', array(
    'section' => 'total_plus_highlight_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 4,
        'step' => 1,
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_title_style', array(
    'selector' => '.ht-highlight-section',
    'render_callback' => 'total_plus_highlight_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_super_title', array(
    'selector' => '.ht-highlight-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_highlight_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_title', array(
    'selector' => '.ht-highlight-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_highlight_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_sub_title', array(
    'selector' => '.ht-highlight-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_highlight_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight', array(
    'selector' => '.ht-highlight-content',
    'render_callback' => 'total_plus_highlight_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_style', array(
    'selector' => '.ht-highlight-content',
    'render_callback' => 'total_plus_highlight_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_col', array(
    'selector' => '.ht-highlight-content',
    'render_callback' => 'total_plus_highlight_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_button_text', array(
    'selector' => '.ht-highlight-section',
    'render_callback' => 'total_plus_highlight_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_highlight_button_link', array(
    'selector' => '.ht-highlight-section',
    'render_callback' => 'total_plus_highlight_section',
    'container_inclusive' => true
));

/* ============PORTFOLIO SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_portfolio_section', array(
    'title' => esc_html__('Portfolio Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_portfolio_section'),
    'hiding_control' => 'total_plus_portfolio_section_disable'
)));

$wp_customize->add_setting('total_plus_portfolio_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_portfolio_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_portfolio_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_portfolio_section_disable',
                'total_plus_portfolio_title_sec_heading',
                'total_plus_portfolio_super_title',
                'total_plus_portfolio_title',
                'total_plus_portfolio_sub_title',
                'total_plus_portfolio_title_style',
                'total_plus_portfolio_cat_heading',
                'total_plus_portfolio_cat',
                'total_plus_portfolio_show_all',
                'total_plus_portfolio_active_cat',
                'total_plus_portfolio_button_text',
                'total_plus_portfolio_button_link',
                'total_plus_portfolio_setting_heading',
                'total_plus_portfolio_cat_menu',
                'total_plus_portfolio_tab_style',
                'total_plus_portfolio_style',
                'total_plus_portfolio_orderby',
                'total_plus_portfolio_order',
                'total_plus_portfolio_full_width',
                'total_plus_portfolio_gap',
                'total_plus_portfolio_zoom',
                'total_plus_portfolio_link'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_portfolio_enable_fullwindow',
                'total_plus_portfolio_align_item',
                'total_plus_portfolio_bg_type',
                'total_plus_portfolio_bg_color',
                'total_plus_portfolio_bg_gradient',
                'total_plus_portfolio_bg_image',
                'total_plus_portfolio_parallax_effect',
                'total_plus_portfolio_bg_video',
                'total_plus_portfolio_overlay_color',
                'total_plus_portfolio_cs_heading',
                'total_plus_portfolio_super_title_color',
                'total_plus_portfolio_title_color',
                'total_plus_portfolio_text_color',
                'total_plus_portfolio_link_color',
                'total_plus_portfolio_link_hov_color',
                'total_plus_portfolio_mb_bg_color',
                'total_plus_portfolio_mb_text_color',
                'total_plus_portfolio_mb_hov_bg_color',
                'total_plus_portfolio_mb_hov_text_color',
                'total_plus_portfolio_cs_seperator',
                'total_plus_portfolio_padding_top',
                'total_plus_portfolio_padding_bottom',
                'total_plus_portfolio_section_seperator',
                'total_plus_portfolio_seperator1',
                'total_plus_portfolio_top_seperator',
                'total_plus_portfolio_ts_color',
                'total_plus_portfolio_ts_height',
                'total_plus_portfolio_seperator2',
                'total_plus_portfolio_bottom_seperator',
                'total_plus_portfolio_bs_color',
                'total_plus_portfolio_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE PORTFOLIO
$wp_customize->add_setting('total_plus_portfolio_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_portfolio_section_disable', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_portfolio_title_sec_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_portfolio_title_sec_heading', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_super_title', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_portfolio_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Portfolio Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_title', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_portfolio_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Portfolio Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_sub_title', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_portfolio_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_title_style', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => array(
        'ht-section-title-top-center' => esc_html__('Top Center Aligned', 'total-plus'),
        'ht-section-title-top-cs' => esc_html__('Top Center Aligned with Seperator', 'total-plus'),
        'ht-section-title-top-left' => esc_html__('Top Left Aligned', 'total-plus'),
        'ht-section-title-top-ls' => esc_html__('Top Left Aligned with Seperator', 'total-plus'),
        'ht-section-title-single-row' => esc_html__('Top Single Row', 'total-plus'),
        'ht-section-title-big' => esc_html__('Top Center Aligned with Big Super Title', 'total-plus')
    )
));

//PORTFOLIO CHOICES
$wp_customize->add_setting('total_plus_portfolio_cat_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_portfolio_cat_heading', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Portfolio Category', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_cat', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Customize_Checkbox_Multiple($wp_customize, 'total_plus_portfolio_cat', array(
    'label' => esc_html__('Select Category', 'total-plus'),
    'section' => 'total_plus_portfolio_section',
    'choices' => $total_plus_portfolio_cat
)));

$wp_customize->add_setting('total_plus_portfolio_show_all', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_show_all', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('All', 'total-plus')
)));

$total_plus_portfolio_cat['*'] = esc_html__('All', 'total-plus');

$wp_customize->add_setting('total_plus_portfolio_active_cat', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => '*',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_active_cat', array(
    'type' => 'select',
    'label' => esc_html__('Active Category', 'total-plus'),
    'section' => 'total_plus_portfolio_section',
    'choices' => $total_plus_portfolio_cat
));

$wp_customize->add_setting('total_plus_portfolio_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_button_text', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_portfolio_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_button_link', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_portfolio_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_portfolio_setting_heading', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_cat_menu', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_cat_menu', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Show Tab', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_tab_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_portfolio_tab_style', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Portfolio Tab Style', 'total-plus'),
    'class' => 'ht-full-width',
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/portfolio-tab-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/portfolio-tab-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/portfolio-tab-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/portfolio-tab-style4.png',
    )
)));

$wp_customize->add_setting('total_plus_portfolio_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_portfolio_style', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Portfolio Masonary Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/portfolio-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/portfolio-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/portfolio-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/portfolio-style4.png',
        'style5' => $imagepath . '/inc/customizer/images/portfolio-style5.png',
        'style6' => $imagepath . '/inc/customizer/images/portfolio-style6.png',
    )
)));

$wp_customize->add_setting('total_plus_portfolio_orderby', array(
    'default' => 'date',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_orderby', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'select',
    'label' => esc_html__('Portfolio Order By', 'total-plus'),
    'choices' => array(
        'title' => esc_html__('Post Title', 'total-plus'),
        'date' => esc_html__('Posted Dated', 'total-plus'),
        'rand' => esc_html__('Random', 'total-plus')
    )
));

$wp_customize->add_setting('total_plus_portfolio_order', array(
    'default' => 'DESC',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_portfolio_order', array(
    'section' => 'total_plus_portfolio_section',
    'type' => 'select',
    'label' => esc_html__('Portfolio Order', 'total-plus'),
    'choices' => array(
        'ASC' => esc_html__('Ascending Order', 'total-plus'),
        'DESC' => esc_html__('Descending Order', 'total-plus')
    )
));

$wp_customize->add_setting('total_plus_portfolio_full_width', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => false,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_full_width', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Full Width', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_gap', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_gap', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Portfolio Gap', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_zoom', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_zoom', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Show Zoom Button', 'total-plus')
)));

$wp_customize->add_setting('total_plus_portfolio_link', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_portfolio_link', array(
    'section' => 'total_plus_portfolio_section',
    'label' => esc_html__('Show Link Button', 'total-plus')
)));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_title_style', array(
    'selector' => '.ht-portfolio-section',
    'render_callback' => 'total_plus_portfolio_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_super_title', array(
    'selector' => '.ht-portfolio-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_portfolio_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_title', array(
    'selector' => '.ht-portfolio-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_portfolio_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_sub_title', array(
    'selector' => '.ht-portfolio-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_portfolio_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_cat', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_show_all', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_active_cat', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_cat_menu', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_tab_style', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_style', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_orderby', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_order', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_full_width', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_gap', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_zoom', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_link', array(
    'selector' => '.ht-portfolio-content',
    'render_callback' => 'total_plus_portfolio_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_button_text', array(
    'selector' => '.ht-portfolio-section',
    'render_callback' => 'total_plus_portfolio_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_portfolio_button_link', array(
    'selector' => '.ht-portfolio-section',
    'render_callback' => 'total_plus_portfolio_section',
    'container_inclusive' => true
));

/* ============SERVICE SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_service_section', array(
    'title' => esc_html__('Service Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_service_section'),
    'hiding_control' => 'total_plus_service_section_disable'
)));

$wp_customize->add_setting('total_plus_service_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_service_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_service_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_service_section_disable',
                'total_plus_service_section_heading',
                'total_plus_service_super_title',
                'total_plus_service_title',
                'total_plus_service_sub_title',
                'total_plus_service_title_style',
                'total_plus_service_header',
                'total_plus_service',
                'total_plus_service_button_text',
                'total_plus_service_button_link',
                'total_plus_service_bg_heading',
                'total_plus_service_bg',
                'total_plus_service_bg_align',
                'total_plus_service_setting_heading',
                'total_plus_service_style'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_service_enable_fullwindow',
                'total_plus_service_align_item',
                'total_plus_service_bg_type',
                'total_plus_service_bg_color',
                'total_plus_service_bg_gradient',
                'total_plus_service_bg_image',
                'total_plus_service_parallax_effect',
                'total_plus_service_bg_video',
                'total_plus_service_overlay_color',
                'total_plus_service_cs_heading',
                'total_plus_service_super_title_color',
                'total_plus_service_title_color',
                'total_plus_service_text_color',
                'total_plus_service_link_color',
                'total_plus_service_link_hov_color',
                'total_plus_service_mb_bg_color',
                'total_plus_service_mb_text_color',
                'total_plus_service_mb_hov_bg_color',
                'total_plus_service_mb_hov_text_color',
                'total_plus_service_cs_seperator',
                'total_plus_service_padding_top',
                'total_plus_service_padding_bottom',
                'total_plus_service_section_seperator',
                'total_plus_service_seperator1',
                'total_plus_service_top_seperator',
                'total_plus_service_ts_color',
                'total_plus_service_ts_height',
                'total_plus_service_seperator2',
                'total_plus_service_bottom_seperator',
                'total_plus_service_bs_color',
                'total_plus_service_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE SERVICE SECTION
$wp_customize->add_setting('total_plus_service_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_service_section_disable', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_service_section_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_service_section_heading', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_service_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_super_title', array(
    'section' => 'total_plus_service_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_service_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Service Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_title', array(
    'section' => 'total_plus_service_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_service_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Service Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_sub_title', array(
    'section' => 'total_plus_service_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_service_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_title_style', array(
    'section' => 'total_plus_service_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => array(
        'ht-section-title-top-center' => esc_html__('Top Center Aligned', 'total-plus'),
        'ht-section-title-top-cs' => esc_html__('Top Center Aligned with Seperator', 'total-plus'),
        'ht-section-title-top-left' => esc_html__('Top Left Aligned', 'total-plus'),
        'ht-section-title-top-ls' => esc_html__('Top Left Aligned with Seperator', 'total-plus'),
        'ht-section-title-big' => esc_html__('Top Center Aligned with Big Title', 'total-plus')
    )
));

//SERVICE PAGES

$wp_customize->add_setting('total_plus_service_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_service_header', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Service Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_service', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'icon' => 'icofont-angry-monster',
            'title' => '',
            'content' => '',
            'link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_service', array(
    'section' => 'total_plus_service_section',
    'box_label' => esc_html__('Service Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'icon' => array(
        'type' => 'icon',
        'label' => esc_html__('Select Icon', 'total-plus'),
        'default' => 'icofont-angry-monster'
    ),
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Content', 'total-plus'),
        'default' => ''
    ),
    'link' => array(
        'type' => 'text',
        'label' => esc_html__('Link', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_service_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_button_text', array(
    'section' => 'total_plus_service_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_service_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_button_link', array(
    'section' => 'total_plus_service_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_service_bg_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_service_bg_heading', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Service Image', 'total-plus')
)));

// Registers example_background settings
$wp_customize->add_setting('total_plus_service_bg_url', array(
    'sanitize_callback' => 'esc_url_raw'
));

$wp_customize->add_setting('total_plus_service_bg_id', array(
    'sanitize_callback' => 'absint'
));

$wp_customize->add_setting('total_plus_service_bg_repeat', array(
    'default' => 'no-repeat',
    'sanitize_callback' => 'sanitize_text_field'
));

$wp_customize->add_setting('total_plus_service_bg_size', array(
    'default' => 'auto',
    'sanitize_callback' => 'sanitize_text_field'
));

$wp_customize->add_setting('total_plus_service_bg_pos', array(
    'default' => 'center-center',
    'sanitize_callback' => 'sanitize_text_field'
));

$wp_customize->add_setting('total_plus_service_bg_attach', array(
    'default' => 'scroll',
    'sanitize_callback' => 'sanitize_text_field'
));

// Registers example_background control
$wp_customize->add_control(new Total_Plus_Background_Control($wp_customize, 'total_plus_service_bg', array(
    'label' => esc_html__('Background Image', 'total-plus'),
    'section' => 'total_plus_service_section',
    'settings' => array(
        'image_url' => 'total_plus_service_bg_url',
        'image_id' => 'total_plus_service_bg_id',
        'repeat' => 'total_plus_service_bg_repeat', // Use false to hide the field
        'size' => 'total_plus_service_bg_size',
        'position' => 'total_plus_service_bg_pos',
        'attach' => 'total_plus_service_bg_attach'
    )
)));

$wp_customize->add_setting('total_plus_service_bg_align', array(
    'default' => 'right',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_service_bg_align', array(
    'section' => 'total_plus_service_section',
    'type' => 'radio',
    'label' => esc_html__('Image Position', 'total-plus'),
    'choices' => array(
        'left' => esc_html__('Left', 'total-plus'),
        'right' => esc_html__('Right', 'total-plus')
    )
));

$wp_customize->add_setting('total_plus_service_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_service_setting_heading', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_service_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_service_style', array(
    'section' => 'total_plus_service_section',
    'label' => esc_html__('Service Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/service-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/service-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/service-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/service-style4.png'
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_service_title_style', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_super_title', array(
    'selector' => '.ht-service-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_service_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_title', array(
    'selector' => '.ht-service-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_service_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_sub_title', array(
    'selector' => '.ht-service-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_service_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service', array(
    'selector' => '.ht-service-post-holder',
    'render_callback' => 'total_plus_service_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_url', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_size', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_repeat', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_attach', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_pos', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_bg_align', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_style', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_button_text', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_service_button_link', array(
    'selector' => '.ht-service-section',
    'render_callback' => 'total_plus_service_section',
    'container_inclusive' => true
));

/* ============TEAM SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_team_section', array(
    'title' => esc_html__('Team Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_team_section'),
    'hiding_control' => 'total_plus_team_section_disable'
)));

$wp_customize->add_setting('total_plus_team_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_team_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_team_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_team_section_disable',
                'total_plus_team_title_subtitle_heading',
                'total_plus_team_super_title',
                'total_plus_team_title',
                'total_plus_team_sub_title',
                'total_plus_team_title_style',
                'total_plus_team_header',
                'total_plus_team',
                'total_plus_team_button_text',
                'total_plus_team_button_link',
                'total_plus_team_setting_heading',
                'total_plus_team_style',
                'total_plus_team_col',
                'total_plus_team_slider_enable'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_team_enable_fullwindow',
                'total_plus_team_align_item',
                'total_plus_team_bg_type',
                'total_plus_team_bg_color',
                'total_plus_team_bg_gradient',
                'total_plus_team_bg_image',
                'total_plus_team_parallax_effect',
                'total_plus_team_bg_video',
                'total_plus_team_overlay_color',
                'total_plus_team_cs_heading',
                'total_plus_team_super_title_color',
                'total_plus_team_title_color',
                'total_plus_team_text_color',
                'total_plus_team_link_color',
                'total_plus_team_link_hov_color',
                'total_plus_team_mb_bg_color',
                'total_plus_team_mb_text_color',
                'total_plus_team_mb_hov_bg_color',
                'total_plus_team_mb_hov_text_color',
                'total_plus_team_cs_seperator',
                'total_plus_team_padding_top',
                'total_plus_team_padding_bottom',
                'total_plus_team_section_seperator',
                'total_plus_team_seperator1',
                'total_plus_team_top_seperator',
                'total_plus_team_ts_color',
                'total_plus_team_ts_height',
                'total_plus_team_seperator2',
                'total_plus_team_bottom_seperator',
                'total_plus_team_bs_color',
                'total_plus_team_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE TEAM SECTION
$wp_customize->add_setting('total_plus_team_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_team_section_disable', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_team_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_team_title_subtitle_heading', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_team_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_super_title', array(
    'section' => 'total_plus_team_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_team_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Team Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_title', array(
    'section' => 'total_plus_team_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_team_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Team Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_sub_title', array(
    'section' => 'total_plus_team_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_team_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_title_style', array(
    'section' => 'total_plus_team_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

$wp_customize->add_setting('total_plus_team_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_team_header', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Team Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_team', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'image' => '',
            'name' => '',
            'designation' => '',
            'content' => '',
            'facebook_link' => '',
            'twitter_link' => '',
            'google_plus_link' => '',
            'link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_team', array(
    'section' => 'total_plus_team_section',
    'box_label' => esc_html__('Team Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Image', 'total-plus'),
        'default' => ''
    ),
    'name' => array(
        'type' => 'text',
        'label' => esc_html__('Name', 'total-plus'),
        'default' => ''
    ),
    'designation' => array(
        'type' => 'text',
        'label' => esc_html__('Designation', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Short Detail', 'total-plus'),
        'default' => ''
    ),
    'link' => array(
        'type' => 'text',
        'label' => esc_html__('Detail Page Link', 'total-plus'),
        'default' => ''
    ),
    'facebook_link' => array(
        'type' => 'text',
        'label' => esc_html__('Facebook Url', 'total-plus'),
        'default' => ''
    ),
    'twitter_link' => array(
        'type' => 'text',
        'label' => esc_html__('Twitter Url', 'total-plus'),
        'default' => ''
    ),
    'google_plus_link' => array(
        'type' => 'text',
        'label' => esc_html__('Google Plus Url', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_team_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_button_text', array(
    'section' => 'total_plus_team_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_team_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_team_button_link', array(
    'section' => 'total_plus_team_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_team_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_team_setting_heading', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_team_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_team_style', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Team Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/team-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/team-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/team-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/team-style4.png',
        'style5' => $imagepath . '/inc/customizer/images/team-style5.png',
        'style6' => $imagepath . '/inc/customizer/images/team-style6.png',
    )
)));

$wp_customize->add_setting('total_plus_team_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_team_col', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 4,
        'step' => 1,
    )
)));

$wp_customize->add_setting('total_plus_team_slider_enable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_team_slider_enable', array(
    'section' => 'total_plus_team_section',
    'label' => esc_html__('Enable Carousel Slider', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_team_title_style', array(
    'selector' => '.ht-team-section',
    'render_callback' => 'total_plus_team_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_team_super_title', array(
    'selector' => '.ht-team-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_team_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_team_title', array(
    'selector' => '.ht-team-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_team_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_team_sub_title', array(
    'selector' => '.ht-team-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_team_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_team', array(
    'selector' => '.ht-team-content',
    'render_callback' => 'total_plus_team_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_team_style', array(
    'selector' => '.ht-team-content',
    'render_callback' => 'total_plus_team_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_team_col', array(
    'selector' => '.ht-team-content',
    'render_callback' => 'total_plus_team_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_team_slider_enable', array(
    'selector' => '.ht-team-content',
    'render_callback' => 'total_plus_team_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_team_button_text', array(
    'selector' => '.ht-team-section',
    'render_callback' => 'total_plus_team_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_team_button_link', array(
    'selector' => '.ht-team-section',
    'render_callback' => 'total_plus_team_section',
    'container_inclusive' => true
));

/* ============PRICING SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_pricing_section', array(
    'title' => esc_html__('Pricing Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_pricing_section'),
    'hiding_control' => 'total_plus_pricing_section_disable'
)));

$wp_customize->add_setting('total_plus_pricing_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_pricing_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_pricing_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_pricing_section_disable',
                'total_plus_pricing_title_subtitle_heading',
                'total_plus_pricing_super_title',
                'total_plus_pricing_title',
                'total_plus_pricing_sub_title',
                'total_plus_pricing_title_style',
                'total_plus_pricing_header',
                'total_plus_pricing',
                'total_plus_pricing_button_text',
                'total_plus_pricing_button_link',
                'total_plus_pricing_setting_heading',
                'total_plus_pricing_style',
                'total_plus_pricing_col'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_pricing_enable_fullwindow',
                'total_plus_pricing_align_item',
                'total_plus_pricing_text_color',
                'total_plus_pricing_bg_type',
                'total_plus_pricing_bg_color',
                'total_plus_pricing_bg_gradient',
                'total_plus_pricing_bg_image',
                'total_plus_pricing_parallax_effect',
                'total_plus_pricing_bg_video',
                'total_plus_pricing_overlay_color',
                'total_plus_pricing_cs_heading',
                'total_plus_pricing_super_title_color',
                'total_plus_pricing_title_color',
                'total_plus_pricing_text_color',
                'total_plus_pricing_link_color',
                'total_plus_pricing_link_hov_color',
                'total_plus_pricing_mb_bg_color',
                'total_plus_pricing_mb_text_color',
                'total_plus_pricing_mb_hov_bg_color',
                'total_plus_pricing_mb_hov_text_color',
                'total_plus_pricing_cs_seperator',
                'total_plus_pricing_padding_top',
                'total_plus_pricing_padding_bottom',
                'total_plus_pricing_section_seperator',
                'total_plus_pricing_seperator1',
                'total_plus_pricing_top_seperator',
                'total_plus_pricing_ts_color',
                'total_plus_pricing_ts_height',
                'total_plus_pricing_seperator2',
                'total_plus_pricing_bottom_seperator',
                'total_plus_pricing_bs_color',
                'total_plus_pricing_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE pricing SECTION
$wp_customize->add_setting('total_plus_pricing_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_pricing_section_disable', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_pricing_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_pricing_title_subtitle_heading', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_pricing_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_super_title', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_pricing_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Pricing Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_title', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_pricing_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Pricing Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_sub_title', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_pricing_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_title_style', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

$wp_customize->add_setting('total_plus_pricing_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_pricing_header', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('Pricing Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_pricing', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'plan' => '',
            'currency' => '',
            'price' => '',
            'price_per' => '',
            'content' => '',
            'button_text' => '',
            'button_link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_pricing', array(
    'section' => 'total_plus_pricing_section',
    'box_label' => esc_html__('Pricing Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'plan' => array(
        'type' => 'text',
        'label' => esc_html__('Pricing Title', 'total-plus'),
        'default' => ''
    ),
    'currency' => array(
        'type' => 'text',
        'label' => esc_html__('Currency Symbol', 'total-plus'),
        'default' => ''
    ),
    'price' => array(
        'type' => 'text',
        'label' => esc_html__('Price', 'total-plus'),
        'default' => ''
    ),
    'price_per' => array(
        'type' => 'text',
        'label' => esc_html__('Price Per(/month, /year)', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Plan Feature List', 'total-plus'),
        'default' => '',
        'description' => esc_html__('Enter Feature list seperated by Enter', 'total-plus'),
    ),
    'button_text' => array(
        'type' => 'text',
        'label' => esc_html__('Button Text', 'total-plus'),
        'default' => ''
    ),
    'button_link' => array(
        'type' => 'text',
        'label' => esc_html__('Button Link', 'total-plus'),
        'default' => ''
    ),
    'is_featured' => array(
        'type' => 'checkbox',
        'label' => esc_html__('Is Featured?', 'total-plus'),
        'default' => 'no'
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_pricing_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_button_text', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_pricing_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_pricing_button_link', array(
    'section' => 'total_plus_pricing_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_pricing_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_pricing_setting_heading', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_pricing_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_pricing_style', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('Pricing Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/pricing-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/pricing-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/pricing-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/pricing-style4.png',
    )
)));

$wp_customize->add_setting('total_plus_pricing_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_pricing_col', array(
    'section' => 'total_plus_pricing_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 4,
        'step' => 1,
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_title_style', array(
    'selector' => '.ht-pricing-section',
    'render_callback' => 'total_plus_pricing_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_super_title', array(
    'selector' => '.ht-pricing-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_pricing_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_title', array(
    'selector' => '.ht-pricing-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_pricing_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_sub_title', array(
    'selector' => '.ht-pricing-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_pricing_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing', array(
    'selector' => '.ht-pricing-content',
    'render_callback' => 'total_plus_pricing_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_style', array(
    'selector' => '.ht-pricing-content',
    'render_callback' => 'total_plus_pricing_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_col', array(
    'selector' => '.ht-pricing-content',
    'render_callback' => 'total_plus_pricing_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_button_text', array(
    'selector' => '.ht-pricing-section',
    'render_callback' => 'total_plus_pricing_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_pricing_button_link', array(
    'selector' => '.ht-pricing-section',
    'render_callback' => 'total_plus_pricing_section',
    'container_inclusive' => true
));

/* ============UPDATE SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_news_section', array(
    'title' => esc_html__('News & Update Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_news_section'),
    'hiding_control' => 'total_plus_news_section_disable'
)));

$wp_customize->add_setting('total_plus_news_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_news_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_news_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_news_section_disable',
                'total_plus_news_section_heading',
                'total_plus_news_super_title',
                'total_plus_news_title',
                'total_plus_news_sub_title',
                'total_plus_news_title_style',
                'total_plus_news_header',
                'total_plus_news',
                'total_plus_news_button_text',
                'total_plus_news_button_link',
                'total_plus_news_setting_heading',
                'total_plus_news_style',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_news_enable_fullwindow',
                'total_plus_news_align_item',
                'total_plus_news_bg_type',
                'total_plus_news_bg_color',
                'total_plus_news_bg_gradient',
                'total_plus_news_bg_image',
                'total_plus_news_parallax_effect',
                'total_plus_news_bg_video',
                'total_plus_news_overlay_color',
                'total_plus_news_cs_heading',
                'total_plus_news_super_title_color',
                'total_plus_news_title_color',
                'total_plus_news_text_color',
                'total_plus_news_link_color',
                'total_plus_news_link_hov_color',
                'total_plus_news_mb_bg_color',
                'total_plus_news_mb_text_color',
                'total_plus_news_mb_hov_bg_color',
                'total_plus_news_mb_hov_text_color',
                'total_plus_news_cs_seperator',
                'total_plus_news_padding_top',
                'total_plus_news_padding_bottom',
                'total_plus_news_section_seperator',
                'total_plus_news_seperator1',
                'total_plus_news_top_seperator',
                'total_plus_news_ts_color',
                'total_plus_news_ts_height',
                'total_plus_news_seperator2',
                'total_plus_news_bottom_seperator',
                'total_plus_news_bs_color',
                'total_plus_news_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE SERVICE SECTION
$wp_customize->add_setting('total_plus_news_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_news_section_disable', array(
    'section' => 'total_plus_news_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_news_section_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_news_section_heading', array(
    'section' => 'total_plus_news_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_news_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_super_title', array(
    'section' => 'total_plus_news_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_news_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('News and Update Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_title', array(
    'section' => 'total_plus_news_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_news_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('News and Update Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_sub_title', array(
    'section' => 'total_plus_news_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_news_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_title_style', array(
    'section' => 'total_plus_news_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => array(
        'ht-section-title-top-center' => esc_html__('Top Center Aligned', 'total-plus'),
        'ht-section-title-top-cs' => esc_html__('Top Center Aligned with Seperator', 'total-plus'),
        'ht-section-title-top-left' => esc_html__('Top Left Aligned', 'total-plus'),
        'ht-section-title-top-ls' => esc_html__('Top Left Aligned with Seperator', 'total-plus'),
        'ht-section-title-single-row' => esc_html__('Top Single Row', 'total-plus'),
        'ht-section-title-big' => esc_html__('Top Center Aligned with Big Title', 'total-plus')
    )
));

//UPDATES PAGES

$wp_customize->add_setting('total_plus_news_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_news_header', array(
    'section' => 'total_plus_news_section',
    'label' => esc_html__('Updates Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_news', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'title' => '',
            'content' => '',
            'image' => '',
            'button_text' => '',
            'button_link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_news', array(
    'section' => 'total_plus_news_section',
    'box_label' => esc_html__('Updates Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Content', 'total-plus'),
        'default' => ''
    ),
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Image', 'total-plus'),
        'default' => ''
    ),
    'button_text' => array(
        'type' => 'text',
        'label' => esc_html__('Button Text', 'total-plus'),
        'default' => ''
    ),
    'button_link' => array(
        'type' => 'text',
        'label' => esc_html__('Button Link', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_news_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_button_text', array(
    'section' => 'total_plus_news_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_news_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_news_button_link', array(
    'section' => 'total_plus_news_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_news_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_news_setting_heading', array(
    'section' => 'total_plus_news_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_news_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_news_style', array(
    'section' => 'total_plus_news_section',
    'label' => esc_html__('Updates Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/news-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/news-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/news-style3.png'
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_news_title_style', array(
    'selector' => '.ht-news-section',
    'render_callback' => 'total_plus_news_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_news_super_title', array(
    'selector' => '.ht-news-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_news_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_news_title', array(
    'selector' => '.ht-news-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_news_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_news_sub_title', array(
    'selector' => '.ht-news-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_news_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_news', array(
    'selector' => '.ht-newscontent',
    'render_callback' => 'total_plus_news_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_news_style', array(
    'selector' => '.ht-newscontent',
    'render_callback' => 'total_plus_news_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_news_button_text', array(
    'selector' => '.ht-news-section',
    'render_callback' => 'total_plus_news_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_news_button_link', array(
    'selector' => '.ht-news-section',
    'render_callback' => 'total_plus_news_section',
    'container_inclusive' => true
));

/* ============TAB SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_tab_section', array(
    'title' => esc_html__('Tab Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_tab_section'),
    'hiding_control' => 'total_plus_tab_section_disable'
)));

$wp_customize->add_setting('total_plus_tab_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_tab_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_tab_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_tab_section_disable',
                'total_plus_tab_section_heading',
                'total_plus_tab_super_title',
                'total_plus_tab_title',
                'total_plus_tab_sub_title',
                'total_plus_tab_title_style',
                'total_plus_tab_header',
                'total_plus_tabs',
                'total_plus_tab_setting_heading',
                'total_plus_tab_style',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_tab_design_settings',
                'total_plus_tab_enable_fullwindow',
                'total_plus_tab_align_item',
                'total_plus_tab_bg_type',
                'total_plus_tab_bg_color',
                'total_plus_tab_bg_gradient',
                'total_plus_tab_bg_image',
                'total_plus_tab_parallax_effect',
                'total_plus_tab_bg_video',
                'total_plus_tab_overlay_color',
                'total_plus_tab_cs_heading',
                'total_plus_tab_super_title_color',
                'total_plus_tab_title_color',
                'total_plus_tab_text_color',
                'total_plus_tab_link_color',
                'total_plus_tab_link_hov_color',
                'total_plus_tab_cs_seperator',
                'total_plus_tab_padding_top',
                'total_plus_tab_padding_bottom',
                'total_plus_tab_section_seperator',
                'total_plus_tab_seperator1',
                'total_plus_tab_top_seperator',
                'total_plus_tab_ts_color',
                'total_plus_tab_ts_height',
                'total_plus_tab_seperator2',
                'total_plus_tab_bottom_seperator',
                'total_plus_tab_bs_color',
                'total_plus_tab_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE SERVICE SECTION
$wp_customize->add_setting('total_plus_tab_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_tab_section_disable', array(
    'section' => 'total_plus_tab_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_tab_section_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_tab_section_heading', array(
    'section' => 'total_plus_tab_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_tab_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_tab_super_title', array(
    'section' => 'total_plus_tab_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_tab_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Tab Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_tab_title', array(
    'section' => 'total_plus_tab_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_tab_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Tab Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_tab_sub_title', array(
    'section' => 'total_plus_tab_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_tab_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_tab_title_style', array(
    'section' => 'total_plus_tab_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => array(
        'ht-section-title-top-center' => esc_html__('Top Center Aligned', 'total-plus'),
        'ht-section-title-top-cs' => esc_html__('Top Center Aligned with Seperator', 'total-plus'),
        'ht-section-title-top-left' => esc_html__('Top Left Aligned', 'total-plus'),
        'ht-section-title-top-ls' => esc_html__('Top Left Aligned with Seperator', 'total-plus'),
        'ht-section-title-single-row' => esc_html__('Top Single Row', 'total-plus'),
        'ht-section-title-big' => esc_html__('Top Center Aligned with Big Title', 'total-plus')
    )
));

//TABS PAGES

$wp_customize->add_setting('total_plus_tab_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_tab_header', array(
    'section' => 'total_plus_tab_section',
    'label' => esc_html__('Tab Blocks', 'total-plus')
)));

$wp_customize->add_setting('total_plus_tabs', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'icon' => '',
            'title' => '',
            'page' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_tabs', array(
    'section' => 'total_plus_tab_section',
    'box_label' => esc_html__('Tabs Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'icon' => array(
        'type' => 'icon',
        'label' => esc_html__('Icon', 'total-plus'),
        'default' => ''
    ),
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'page' => array(
        'type' => 'select',
        'label' => esc_html__('Select Page', 'total-plus'),
        'options' => $total_plus_page_choice,
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_tab_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_tab_setting_heading', array(
    'section' => 'total_plus_tab_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_tab_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_tab_style', array(
    'section' => 'total_plus_tab_section',
    'label' => esc_html__('Tab Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/tab-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/tab-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/tab-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/tab-style4.png',
        'style5' => $imagepath . '/inc/customizer/images/tab-style5.png'
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_tab_title_style', array(
    'selector' => '.ht-tab-section',
    'render_callback' => 'total_plus_tab_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_tab_super_title', array(
    'selector' => '.ht-tab-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_tab_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_tab_title', array(
    'selector' => '.ht-tab-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_tab_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_tab_sub_title', array(
    'selector' => '.ht-tab-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_tab_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_tabs', array(
    'selector' => '.ht-tab-section .ht-section-content',
    'render_callback' => 'total_plus_tab_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_tab_style', array(
    'selector' => '.ht-tab-section .ht-section-content',
    'render_callback' => 'total_plus_tab_content'
));

/* ============COUNTER SECTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_counter_section', array(
    'title' => esc_html__('Counter Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_counter_section'),
    'hiding_control' => 'total_plus_counter_section_disable'
)));

$wp_customize->add_setting('total_plus_counter_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_counter_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_counter_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_counter_section_disable',
                'total_plus_counter_title_subtitle_heading',
                'total_plus_counter_super_title',
                'total_plus_counter_title',
                'total_plus_counter_sub_title',
                'total_plus_counter_title_style',
                'total_plus_counter_heading',
                'total_plus_counter',
                'total_plus_counter_button_text',
                'total_plus_counter_button_link',
                'total_plus_counter_setting_heading',
                'total_plus_counter_style',
                'total_plus_counter_col'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_counter_enable_fullwindow',
                'total_plus_counter_text_color',
                'total_plus_counter_bg_type',
                'total_plus_counter_bg_color',
                'total_plus_counter_bg_gradient',
                'total_plus_counter_bg_image',
                'total_plus_counter_parallax_effect',
                'total_plus_counter_bg_video',
                'total_plus_counter_overlay_color',
                'total_plus_counter_cs_heading',
                'total_plus_counter_super_title_color',
                'total_plus_counter_title_color',
                'total_plus_counter_text_color',
                'total_plus_counter_link_color',
                'total_plus_counter_link_hov_color',
                'total_plus_counter_mb_bg_color',
                'total_plus_counter_mb_text_color',
                'total_plus_counter_mb_hov_bg_color',
                'total_plus_counter_mb_hov_text_color',
                'total_plus_counter_cs_seperator',
                'total_plus_counter_padding_top',
                'total_plus_counter_padding_bottom',
                'total_plus_counter_section_seperator',
                'total_plus_counter_seperator1',
                'total_plus_counter_top_seperator',
                'total_plus_counter_ts_color',
                'total_plus_counter_ts_height',
                'total_plus_counter_seperator2',
                'total_plus_counter_bottom_seperator',
                'total_plus_counter_bs_color',
                'total_plus_counter_bs_height'
            ),
        ),
    ),
)));

$wp_customize->add_setting('total_plus_counter_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

//ENABLE/DISABLE COUNTER SECTION
$wp_customize->add_setting('total_plus_counter_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_counter_section_disable', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_counter_title_subtitle_heading', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_counter_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_super_title', array(
    'section' => 'total_plus_counter_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_counter_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Counter Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_title', array(
    'section' => 'total_plus_counter_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_counter_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Counter Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_sub_title', array(
    'section' => 'total_plus_counter_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_counter_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_title_style', array(
    'section' => 'total_plus_counter_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

$wp_customize->add_setting('total_plus_counter_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_counter_heading', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('Counters', 'total-plus')
)));

$wp_customize->add_setting('total_plus_counter', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'title' => '',
            'value' => '',
            'icon' => 'icofont-angry-monster',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_counter', array(
    'section' => 'total_plus_counter_section',
    'box_label' => esc_html__('Counter Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'title' => array(
        'type' => 'text',
        'label' => esc_html__('Title', 'total-plus'),
        'default' => ''
    ),
    'value' => array(
        'type' => 'text',
        'label' => esc_html__('Counter Value', 'total-plus'),
        'default' => ''
    ),
    'icon' => array(
        'type' => 'icon',
        'label' => esc_html__('Icon', 'total-plus'),
        'default' => 'icofont-angry-monster'
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_counter_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_button_text', array(
    'section' => 'total_plus_counter_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_counter_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_counter_button_link', array(
    'section' => 'total_plus_counter_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_counter_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_counter_setting_heading', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_counter_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_counter_style', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('Counter Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/counter-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/counter-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/counter-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/counter-style4.png'
    )
)));

$wp_customize->add_setting('total_plus_counter_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_counter_col', array(
    'section' => 'total_plus_counter_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 6,
        'step' => 1,
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_counter_title_style', array(
    'selector' => '.ht-counter-section',
    'render_callback' => 'total_plus_counter_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_super_title', array(
    'selector' => '.ht-counter-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_counter_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_title', array(
    'selector' => '.ht-counter-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_counter_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_sub_title', array(
    'selector' => '.ht-counter-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_counter_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_counter', array(
    'selector' => '.ht-counter-content',
    'render_callback' => 'total_plus_counter_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_style', array(
    'selector' => '.ht-counter-content',
    'render_callback' => 'total_plus_counter_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_col', array(
    'selector' => '.ht-counter-content',
    'render_callback' => 'total_plus_counter_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_button_text', array(
    'selector' => '.ht-counter-section',
    'render_callback' => 'total_plus_counter_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_counter_button_link', array(
    'selector' => '.ht-counter-section',
    'render_callback' => 'total_plus_counter_section',
    'container_inclusive' => true
));

/* ============TESTIMONIAL PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_testimonial_section', array(
    'title' => esc_html__('Testimonial Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_testimonial_section'),
    'hiding_control' => 'total_plus_testimonial_section_disable'
)));

$wp_customize->add_setting('total_plus_testimonial_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_testimonial_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_testimonial_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_testimonial_section_disable',
                'total_plus_testimonial_title_subtitle_heading',
                'total_plus_testimonial_super_title',
                'total_plus_testimonial_title',
                'total_plus_testimonial_sub_title',
                'total_plus_testimonial_title_style',
                'total_plus_testimonial_header',
                'total_plus_testimonial',
                'total_plus_testimonial_button_text',
                'total_plus_testimonial_button_link',
                'total_plus_testimonial_setting_heading',
                'total_plus_testimonial_style'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_testimonial_enable_fullwindow',
                'total_plus_testimonial_align_item',
                'total_plus_testimonial_bg_type',
                'total_plus_testimonial_bg_color',
                'total_plus_testimonial_bg_gradient',
                'total_plus_testimonial_bg_image',
                'total_plus_testimonial_parallax_effect',
                'total_plus_testimonial_bg_video',
                'total_plus_testimonial_overlay_color',
                'total_plus_testimonial_cs_heading',
                'total_plus_testimonial_super_title_color',
                'total_plus_testimonial_title_color',
                'total_plus_testimonial_text_color',
                'total_plus_testimonial_link_color',
                'total_plus_testimonial_link_hov_color',
                'total_plus_testimonial_mb_bg_color',
                'total_plus_testimonial_mb_text_color',
                'total_plus_testimonial_mb_hov_bg_color',
                'total_plus_testimonial_mb_hov_text_color',
                'total_plus_testimonial_cs_seperator',
                'total_plus_testimonial_padding_top',
                'total_plus_testimonial_padding_bottom',
                'total_plus_testimonial_section_seperator',
                'total_plus_testimonial_seperator1',
                'total_plus_testimonial_top_seperator',
                'total_plus_testimonial_ts_color',
                'total_plus_testimonial_ts_height',
                'total_plus_testimonial_seperator2',
                'total_plus_testimonial_bottom_seperator',
                'total_plus_testimonial_bs_color',
                'total_plus_testimonial_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE TESTIMONIAL SECTION
$wp_customize->add_setting('total_plus_testimonial_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_testimonial_section_disable', array(
    'section' => 'total_plus_testimonial_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_testimonial_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_testimonial_title_subtitle_heading', array(
    'section' => 'total_plus_testimonial_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_testimonial_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_super_title', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_testimonial_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Testimonial Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_title', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_testimonial_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Testimonial Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_sub_title', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_testimonial_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_title_style', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

//TESTIMONIAL PAGES
$wp_customize->add_setting('total_plus_testimonial_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_testimonial_header', array(
    'section' => 'total_plus_testimonial_section',
    'label' => esc_html__('Testimonial', 'total-plus')
)));

$wp_customize->add_setting('total_plus_testimonial', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'image' => '',
            'name' => '',
            'designation' => '',
            'content' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_testimonial', array(
    'section' => 'total_plus_testimonial_section',
    'box_label' => esc_html__('Testimonial Block', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Image', 'total-plus'),
        'default' => ''
    ),
    'name' => array(
        'type' => 'text',
        'label' => esc_html__('Name', 'total-plus'),
        'default' => ''
    ),
    'designation' => array(
        'type' => 'text',
        'label' => esc_html__('Designation', 'total-plus'),
        'default' => ''
    ),
    'content' => array(
        'type' => 'textarea',
        'label' => esc_html__('Short Detail', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_testimonial_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_button_text', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_testimonial_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_testimonial_button_link', array(
    'section' => 'total_plus_testimonial_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_testimonial_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_testimonial_setting_heading', array(
    'section' => 'total_plus_testimonial_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_testimonial_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_testimonial_style', array(
    'section' => 'total_plus_testimonial_section',
    'label' => esc_html__('Testimonial Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/testimonial-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/testimonial-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/testimonial-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/testimonial-style4.png',
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_title_style', array(
    'selector' => '.ht-testimonial-section',
    'render_callback' => 'total_plus_testimonial_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_super_title', array(
    'selector' => '.ht-testimonial-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_testimonial_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_title', array(
    'selector' => '.ht-testimonial-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_testimonial_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_sub_title', array(
    'selector' => '.ht-testimonial-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_testimonial_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial', array(
    'selector' => '.ht-testimonial-content',
    'render_callback' => 'total_plus_testimonial_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_style', array(
    'selector' => '.ht-testimonial-content',
    'render_callback' => 'total_plus_testimonial_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_button_text', array(
    'selector' => '.ht-testimonial-section',
    'render_callback' => 'total_plus_testimonial_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_testimonial_button_link', array(
    'selector' => '.ht-testimonial-section',
    'render_callback' => 'total_plus_testimonial_section',
    'container_inclusive' => true
));

/* ============BLOG PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_blog_section', array(
    'title' => esc_html__('Blog Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_blog_section'),
    'hiding_control' => 'total_plus_blog_section_disable'
)));

$wp_customize->add_setting('total_plus_blog_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_blog_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_blog_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_blog_section_disable',
                'total_plus_blog_title_subtitle_heading',
                'total_plus_blog_super_title',
                'total_plus_blog_title',
                'total_plus_blog_sub_title',
                'total_plus_blog_title_style',
                'total_plus_blog_cat_exclude',
                'total_plus_blog_setting_heading',
                'total_plus_blog_style',
                'total_plus_blog_col',
                'total_plus_blog_post_count',
                'total_plus_blog_excerpt_count',
                'total_plus_blog_show_date',
                'total_plus_blog_show_author_comment',
                'total_plus_blog_button_text',
                'total_plus_blog_button_link',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_blog_enable_fullwindow',
                'total_plus_blog_align_item',
                'total_plus_blog_bg_type',
                'total_plus_blog_bg_color',
                'total_plus_blog_bg_gradient',
                'total_plus_blog_bg_image',
                'total_plus_blog_parallax_effect',
                'total_plus_blog_bg_video',
                'total_plus_blog_overlay_color',
                'total_plus_blog_cs_heading',
                'total_plus_blog_super_title_color',
                'total_plus_blog_title_color',
                'total_plus_blog_text_color',
                'total_plus_blog_link_color',
                'total_plus_blog_link_hov_color',
                'total_plus_blog_mb_bg_color',
                'total_plus_blog_mb_text_color',
                'total_plus_blog_mb_hov_bg_color',
                'total_plus_blog_mb_hov_text_color',
                'total_plus_blog_cs_seperator',
                'total_plus_blog_padding_top',
                'total_plus_blog_padding_bottom',
                'total_plus_blog_section_seperator',
                'total_plus_blog_seperator1',
                'total_plus_blog_top_seperator',
                'total_plus_blog_ts_color',
                'total_plus_blog_ts_height',
                'total_plus_blog_seperator2',
                'total_plus_blog_bottom_seperator',
                'total_plus_blog_bs_color',
                'total_plus_blog_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE BLOG SECTION
$wp_customize->add_setting('total_plus_blog_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_blog_section_disable', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_blog_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_blog_title_subtitle_heading', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_super_title', array(
    'section' => 'total_plus_blog_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_blog_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Blog Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_title', array(
    'section' => 'total_plus_blog_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_blog_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Blog Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_sub_title', array(
    'section' => 'total_plus_blog_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_blog_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_title_style', array(
    'section' => 'total_plus_blog_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

//BLOG SETTINGS
$wp_customize->add_setting('total_plus_blog_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_blog_setting_heading', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_blog_style', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Blog Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/blog-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/blog-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/blog-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/blog-style4.png',
    )
)));

$wp_customize->add_setting('total_plus_blog_cat_exclude', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Customize_Checkbox_Multiple($wp_customize, 'total_plus_blog_cat_exclude', array(
    'label' => esc_html__('Exclude Category from Blog Posts', 'total-plus'),
    'section' => 'total_plus_blog_section',
    'choices' => $total_plus_cat
)));

$wp_customize->add_setting('total_plus_blog_col', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_blog_col', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('No of Columns', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 4,
        'step' => 1,
    )
)));

$wp_customize->add_setting('total_plus_blog_post_count', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_blog_post_count', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Number of Posts to show', 'total-plus'),
    'options' => array(
        'min' => 2,
        'max' => 15,
        'step' => 1,
    )
)));

$wp_customize->add_setting('total_plus_blog_excerpt_count', array(
    'sanitize_callback' => 'absint',
    'default' => 120,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_blog_excerpt_count', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Excerpt Character', 'total-plus'),
    'options' => array(
        'min' => 0,
        'max' => 600,
        'step' => 10,
    )
)));

$wp_customize->add_setting('total_plus_blog_show_date', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_blog_show_date', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Show Date', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_show_author_comment', array(
    'sanitize_callback' => 'total_plus_sanitize_checkbox',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_blog_show_author_comment', array(
    'section' => 'total_plus_blog_section',
    'label' => esc_html__('Show Author & Comment Count', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_button_text', array(
    'section' => 'total_plus_blog_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_blog_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_blog_button_link', array(
    'section' => 'total_plus_blog_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_title_style', array(
    'selector' => '.ht-blog-section',
    'render_callback' => 'total_plus_blog_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_super_title', array(
    'selector' => '.ht-blog-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_blog_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_title', array(
    'selector' => '.ht-blog-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_blog_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_sub_title', array(
    'selector' => '.ht-blog-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_blog_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_cat_exclude', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_style', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_col', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_post_count', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_excerpt_count', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_show_date', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_show_author_comment', array(
    'selector' => '.ht-blog-content',
    'render_callback' => 'total_plus_blog_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_button_text', array(
    'selector' => '.ht-blog-section',
    'render_callback' => 'total_plus_blog_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_blog_button_link', array(
    'selector' => '.ht-blog-section',
    'render_callback' => 'total_plus_blog_section',
    'container_inclusive' => true
));

/* ============CLIENTS LOGO SECTION============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_logo_section', array(
    'title' => esc_html__('Clients Logo Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_logo_section'),
    'hiding_control' => 'total_plus_logo_section_disable'
)));

$wp_customize->add_setting('total_plus_logo_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_logo_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_logo_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_logo_section_disable',
                'total_plus_logo_title_subtitle_heading',
                'total_plus_logo_super_title',
                'total_plus_logo_title',
                'total_plus_logo_sub_title',
                'total_plus_logo_title_style',
                'total_plus_logo_header',
                'total_plus_logo',
                'total_plus_logo_button_text',
                'total_plus_logo_button_link',
                'total_plus_logo_new_tab',
                'total_plus_logo_setting_heading',
                'total_plus_logo_style'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_logo_enable_fullwindow',
                'total_plus_logo_align_item',
                'total_plus_logo_bg_type',
                'total_plus_logo_bg_color',
                'total_plus_logo_bg_gradient',
                'total_plus_logo_bg_image',
                'total_plus_logo_parallax_effect',
                'total_plus_logo_bg_video',
                'total_plus_logo_overlay_color',
                'total_plus_logo_cs_heading',
                'total_plus_logo_super_title_color',
                'total_plus_logo_title_color',
                'total_plus_logo_text_color',
                'total_plus_logo_link_color',
                'total_plus_logo_link_hov_color',
                'total_plus_logo_mb_bg_color',
                'total_plus_logo_mb_text_color',
                'total_plus_logo_mb_hov_bg_color',
                'total_plus_logo_mb_hov_text_color',
                'total_plus_logo_cs_seperator',
                'total_plus_logo_padding_top',
                'total_plus_logo_padding_bottom',
                'total_plus_logo_section_seperator',
                'total_plus_logo_seperator1',
                'total_plus_logo_top_seperator',
                'total_plus_logo_ts_color',
                'total_plus_logo_ts_height',
                'total_plus_logo_seperator2',
                'total_plus_logo_bottom_seperator',
                'total_plus_logo_bs_color',
                'total_plus_logo_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE LOGO SECTION
$wp_customize->add_setting('total_plus_logo_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_logo_section_disable', array(
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_logo_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_logo_title_subtitle_heading', array(
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_logo_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_super_title', array(
    'section' => 'total_plus_logo_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Client Logo Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_title', array(
    'section' => 'total_plus_logo_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Clients Logo Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_sub_title', array(
    'section' => 'total_plus_logo_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_title_style', array(
    'section' => 'total_plus_logo_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => array(
        'ht-section-title-top-center' => esc_html__('Top Center Aligned', 'total-plus'),
        'ht-section-title-top-cs' => esc_html__('Top Center Aligned with Seperator', 'total-plus'),
        'ht-section-title-top-left' => esc_html__('Top Left Aligned', 'total-plus'),
        'ht-section-title-top-ls' => esc_html__('Top Left Aligned with Seperator', 'total-plus'),
        'ht-section-title-single-row' => esc_html__('Top Single Row', 'total-plus'),
        'ht-section-title-big' => esc_html__('Top Center Aligned with Big Title', 'total-plus')
    )
));

//CLIENTS LOGOS
$wp_customize->add_setting('total_plus_logo_header', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_logo_header', array(
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Clients Logos', 'total-plus')
)));

$wp_customize->add_setting('total_plus_logo', array(
    'sanitize_callback' => 'total_plus_sanitize_repeater',
    'default' => json_encode(array(
        array(
            'image' => '',
            'link' => '',
            'enable' => 'on'
        )
    )),
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Repeater_Controler($wp_customize, 'total_plus_logo', array(
    'section' => 'total_plus_logo_section',
    'box_label' => esc_html__('Clients Logo', 'total-plus'),
    'add_label' => esc_html__('Add New', 'total-plus'),
        ), array(
    'image' => array(
        'type' => 'upload',
        'label' => esc_html__('Upload Logo', 'total-plus'),
        'default' => ''
    ),
    'link' => array(
        'type' => 'text',
        'label' => esc_html__('Logo Link', 'total-plus'),
        'default' => ''
    ),
    'enable' => array(
        'type' => 'switch',
        'label' => esc_html__('Enable Section', 'total-plus'),
        'switch' => array(
            'on' => 'Yes',
            'off' => 'No'
        ),
        'default' => 'on'
    )
)));

$wp_customize->add_setting('total_plus_logo_new_tab', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_new_tab', array(
    'type' => 'checkbox',
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Open Logo Link in New Tab', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_button_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_button_text', array(
    'section' => 'total_plus_logo_section',
    'type' => 'text',
    'label' => esc_html__('More Button Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_button_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_logo_button_link', array(
    'section' => 'total_plus_logo_section',
    'type' => 'text',
    'label' => esc_html__('More Button Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_logo_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_logo_setting_heading', array(
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_logo_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_logo_style', array(
    'section' => 'total_plus_logo_section',
    'label' => esc_html__('Logo Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/logo-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/logo-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/logo-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/logo-style4.png',
    )
)));

$wp_customize->selective_refresh->add_partial('total_plus_logo_title_style', array(
    'selector' => '.ht-logo-section',
    'render_callback' => 'total_plus_logo_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_super_title', array(
    'selector' => '.ht-logo-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_logo_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_title', array(
    'selector' => '.ht-logo-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_logo_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_sub_title', array(
    'selector' => '.ht-logo-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_logo_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_logo', array(
    'selector' => '.ht-logo-content',
    'render_callback' => 'total_plus_logo_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_new_tab', array(
    'selector' => '.ht-logo-content',
    'render_callback' => 'total_plus_logo_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_style', array(
    'selector' => '.ht-logo-content',
    'render_callback' => 'total_plus_logo_content'
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_button_text', array(
    'selector' => '.ht-logo-section',
    'render_callback' => 'total_plus_logo_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_logo_button_link', array(
    'selector' => '.ht-logo-section',
    'render_callback' => 'total_plus_logo_section',
    'container_inclusive' => true
));

/* =============CONTACT US================== */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_contact_section', array(
    'title' => esc_html__('Contact Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_contact_section'),
    'hiding_control' => 'total_plus_contact_section_disable'
)));

$wp_customize->add_setting('total_plus_contact_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post'
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_contact_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_contact_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_contact_section_disable',
                'total_plus_google_map_heading',
                'total_plus_longitude',
                'total_plus_latitude',
                'total_plus_map_style',
                'total_plus_google_map_api',
                'total_plus_enable_iframe_map',
                'total_plus_google_map_iframe',
                'total_plus_contact_details_heading',
                'total_plus_show_contact_detail',
                'total_plus_contact_shortcode',
                'total_plus_contact_detail',
                'total_plus_contact_social_icons',
                'total_plus_contact_social_link'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_contact_enable_fullwindow',
                'total_plus_contact_align_item',
                'total_plus_contact_bg_type',
                'total_plus_contact_bg_color',
                'total_plus_contact_bg_gradient',
                'total_plus_contact_bg_image',
                'total_plus_contact_parallax_effect',
                'total_plus_contact_bg_video',
                'total_plus_contact_overlay_color',
                'total_plus_contact_cs_heading',
                'total_plus_contact_super_title_color',
                'total_plus_contact_title_color',
                'total_plus_contact_text_color',
                'total_plus_contact_link_color',
                'total_plus_contact_link_hov_color',
                'total_plus_contact_cs_seperator',
                'total_plus_contact_padding_top',
                'total_plus_contact_padding_bottom',
                'total_plus_contact_section_seperator',
                'total_plus_contact_seperator1',
                'total_plus_contact_top_seperator',
                'total_plus_contact_ts_color',
                'total_plus_contact_ts_height',
                'total_plus_contact_seperator2',
                'total_plus_contact_bottom_seperator',
                'total_plus_contact_bs_color',
                'total_plus_contact_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE CONTACT SECTION
$wp_customize->add_setting('total_plus_contact_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_contact_section_disable', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_google_map_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_google_map_heading', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Google Map', 'total-plus'),
    'description' => sprintf(esc_html__('Get the Longitude and Latitude value of the location from %s', 'total-plus'), '<a target="_blank" href="https://www.latlong.net/">here</a>')
)));

$wp_customize->add_setting('total_plus_latitude', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => '29.424122',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_latitude', array(
    'section' => 'total_plus_contact_section',
    'type' => 'text',
    'label' => esc_html__('Latitude', 'total-plus')
));

$wp_customize->add_setting('total_plus_longitude', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => '-98.493629',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_longitude', array(
    'section' => 'total_plus_contact_section',
    'type' => 'text',
    'label' => esc_html__('Longitude', 'total-plus')
));

$wp_customize->add_setting('total_plus_map_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'normal',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_map_style', array(
    'section' => 'total_plus_contact_section',
    'type' => 'select',
    'label' => esc_html__('Map Style', 'total-plus'),
    'choices' => array(
        'normal' => esc_html__('Normal', 'total-plus'),
        'light' => esc_html__('Light', 'total-plus'),
        'dark' => esc_html__('Dark', 'total-plus')
    )
));

$wp_customize->add_setting('total_plus_google_map_api', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Info_Text($wp_customize, 'total_plus_google_map_api', array(
    'label' => esc_html__('Google Map API Key', 'total-plus'),
    'section' => 'total_plus_contact_section',
    'description' => sprintf(esc_html__('Google Map API key is required for a map to work. Enter API key %s', 'total-plus'), '<a target="_blank" href="' . admin_url('admin.php?page=total-plus-options') . '">' . esc_html__('Here', 'total-plus') . '</a>')
)));

$wp_customize->add_setting('total_plus_enable_iframe_map', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => false,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_enable_iframe_map', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Enable Iframe Google Map', 'total-plus'),
    'description' => esc_html__('Don\'t have Google API. No Problem. Enable Iframe Google Map and add the Google Map Iframe below.', 'total-plus')
)));

$wp_customize->add_setting('total_plus_google_map_iframe', array(
    //'sanitize_callback' => 'wp_kses_post',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_google_map_iframe', array(
    'section' => 'total_plus_contact_section',
    'type' => 'textarea',
    'label' => esc_html__('Google Map Iframe', 'total-plus')
));

$wp_customize->add_setting('total_plus_contact_details_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_contact_details_heading', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Contact Details', 'total-plus')
)));

$wp_customize->add_setting('total_plus_show_contact_detail', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'on',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_show_contact_detail', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Show Contact Detail', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    )
)));

$wp_customize->add_setting('total_plus_contact_shortcode', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_contact_shortcode', array(
    'section' => 'total_plus_contact_section',
    'type' => 'text',
    'label' => esc_html__('Contact Form Shortcode', 'total-plus'),
    'description' => sprintf(esc_html__('Install %s plugin to get the shortcode', 'total-plus'), '<a target="_blank" href="https://wordpress.org/plugins/contact-form-7/">Contact Form 7</a>')
));

$wp_customize->add_setting('total_plus_contact_detail', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => '',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Page_Editor($wp_customize, 'total_plus_contact_detail', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Contact Detail', 'total-plus'),
    'include_admin_print_footer' => true
)));

$wp_customize->add_setting('total_plus_contact_social_icons', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true,
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_contact_social_icons', array(
    'section' => 'total_plus_contact_section',
    'label' => esc_html__('Show Social Icons Below Contact Detail', 'total-plus')
)));

$wp_customize->add_setting('total_plus_contact_social_link', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Info_Text($wp_customize, 'total_plus_contact_social_link', array(
    'label' => esc_html__('Social Icons', 'total-plus'),
    'section' => 'total_plus_contact_section',
    'description' => sprintf(esc_html__('Add your %s here', 'total-plus'), '<a href="#">Social Icons</a>')
)));

$wp_customize->selective_refresh->add_partial('total_plus_longitude', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_latitude', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_map_style', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_google_map_api', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_enable_iframe_map', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_google_map_iframe', array(
    'selector' => '.ht-contact-google-map',
    'render_callback' => 'total_plus_contact_map',
));

$wp_customize->selective_refresh->add_partial('total_plus_contact_shortcode', array(
    'selector' => '.ht-contact-section .ht-container',
    'render_callback' => 'total_plus_contact_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_contact_detail', array(
    'selector' => '.ht-contact-section .ht-container',
    'render_callback' => 'total_plus_contact_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_contact_social_icons', array(
    'selector' => '.ht-contact-section .ht-container',
    'render_callback' => 'total_plus_contact_content',
));

$wp_customize->selective_refresh->add_partial('total_plus_show_contact_detail', array(
    'selector' => '.ht-contact-section',
    'render_callback' => 'total_plus_contact_section',
    'container_inclusive' => true
));


/* ============CALL TO ACTION PANEL============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_cta_section', array(
    'title' => esc_html__('Call To Action Section', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_cta_section'),
    'hiding_control' => 'total_plus_cta_section_disable'
)));

$wp_customize->add_setting('total_plus_cta_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_cta_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_cta_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_cta_section_disable',
                'total_plus_cta_super_title',
                'total_plus_cta_title',
                'total_plus_cta_sub_title',
                'total_plus_cta_button1_text',
                'total_plus_cta_button1_link',
                'total_plus_cta_button2_text',
                'total_plus_cta_button2_link',
                'total_plus_cta_video_heading',
                'total_plus_cta_video_url',
                'total_plus_cta_video_button_icon',
                'total_plus_cta_setting_heading',
                'total_plus_cta_style'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_cta_enable_fullwindow',
                'total_plus_cta_align_item',
                'total_plus_cta_bg_type',
                'total_plus_cta_bg_color',
                'total_plus_cta_bg_gradient',
                'total_plus_cta_bg_image',
                'total_plus_cta_parallax_effect',
                'total_plus_cta_bg_video',
                'total_plus_cta_overlay_color',
                'total_plus_cta_cs_heading',
                'total_plus_cta_super_title_color',
                'total_plus_cta_title_color',
                'total_plus_cta_text_color',
                'total_plus_cta_link_color',
                'total_plus_cta_link_hov_color',
                'total_plus_cta_button1_bg_color',
                'total_plus_cta_button1_text_color',
                'total_plus_cta_button2_bg_color',
                'total_plus_cta_button2_text_color',
                'total_plus_cta_video_icon_color',
                'total_plus_cta_cs_seperator',
                'total_plus_cta_padding_top',
                'total_plus_cta_padding_bottom',
                'total_plus_cta_section_seperator',
                'total_plus_cta_seperator1',
                'total_plus_cta_top_seperator',
                'total_plus_cta_ts_color',
                'total_plus_cta_ts_height',
                'total_plus_cta_seperator2',
                'total_plus_cta_bottom_seperator',
                'total_plus_cta_bs_color',
                'total_plus_cta_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE LOGO SECTION
$wp_customize->add_setting('total_plus_cta_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_cta_section_disable', array(
    'section' => 'total_plus_cta_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_cta_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_super_title', array(
    'section' => 'total_plus_cta_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Call To Action Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_title', array(
    'section' => 'total_plus_cta_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Call To Action Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_sub_title', array(
    'section' => 'total_plus_cta_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_button1_text', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_button1_text', array(
    'section' => 'total_plus_cta_section',
    'type' => 'text',
    'label' => esc_html__('Button 1 Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_button1_link', array(
    'default' => '',
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_button1_link', array(
    'section' => 'total_plus_cta_section',
    'type' => 'url',
    'label' => esc_html__('Button 1 Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_button2_text', array(
    'default' => '',
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_button2_text', array(
    'section' => 'total_plus_cta_section',
    'type' => 'text',
    'label' => esc_html__('Button 2 Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_button2_link', array(
    'default' => '',
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_button2_link', array(
    'section' => 'total_plus_cta_section',
    'type' => 'url',
    'label' => esc_html__('Button 2 Link', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_video_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_cta_video_heading', array(
    'section' => 'total_plus_cta_section',
    'label' => esc_html__('Video', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_video_url', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_cta_video_url', array(
    'section' => 'total_plus_cta_section',
    'type' => 'text',
    'label' => esc_html__('Video Url', 'total-plus'),
    'description' => esc_html__('To display YouTube, Vimeo or VK video, paste the video URL (https://www.youtube.com/watch?v=6O9Nd1RSZSY)', 'total-plus')
));

$wp_customize->add_setting('total_plus_cta_video_button_icon', array(
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'total_plus_cta_video_button_icon', array(
    'section' => 'total_plus_cta_section',
    'label' => esc_html__('Video Play Icon', 'total-plus'),
    'description' => esc_html__('On clicking the video play button, the video will show in popup. Default play button will show if left empty.', 'total-plus'),
)));

$wp_customize->add_setting('total_plus_cta_setting_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_cta_setting_heading', array(
    'section' => 'total_plus_cta_section',
    'label' => esc_html__('Settings', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_style', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'style1',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Total_Plus_Selector($wp_customize, 'total_plus_cta_style', array(
    'section' => 'total_plus_cta_section',
    'label' => esc_html__('CTA Block Style', 'total-plus'),
    'options' => array(
        'style1' => $imagepath . '/inc/customizer/images/cta-style1.png',
        'style2' => $imagepath . '/inc/customizer/images/cta-style2.png',
        'style3' => $imagepath . '/inc/customizer/images/cta-style3.png',
        'style4' => $imagepath . '/inc/customizer/images/cta-style4.png'
    )
)));

$wp_customize->add_setting('total_plus_cta_button1_bg_color', array(
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_cta_button1_bg_color', array(
    'section' => 'total_plus_cta_section',
    'priority' => 56,
    'label' => esc_html__('Button 1 Background Color', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_button1_text_color', array(
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_cta_button1_text_color', array(
    'section' => 'total_plus_cta_section',
    'priority' => 56,
    'label' => esc_html__('Button 1 Text Color', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_button2_bg_color', array(
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_cta_button2_bg_color', array(
    'section' => 'total_plus_cta_section',
    'priority' => 56,
    'label' => esc_html__('Button 2 Background Color', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_button2_text_color', array(
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_cta_button2_text_color', array(
    'section' => 'total_plus_cta_section',
    'priority' => 56,
    'label' => esc_html__('Button 2 Text Color', 'total-plus')
)));

$wp_customize->add_setting('total_plus_cta_video_icon_color', array(
    'default' => '#e52d27',
    'sanitize_callback' => 'sanitize_hex_color',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'total_plus_cta_video_icon_color', array(
    'section' => 'total_plus_cta_section',
    'priority' => 56,
    'label' => esc_html__('Video Play Button Color', 'total-plus'),
    'description' => esc_html__('Only applies on default play button. Do not choose white color.', 'total-plus'),
)));

$wp_customize->selective_refresh->add_partial('total_plus_cta_super_title', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_title', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_sub_title', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_button1_text', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_button1_link', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_button2_text', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_button2_link', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_style', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_video_url', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_cta_video_button_icon', array(
    'selector' => '.ht-cta-section',
    'render_callback' => 'total_plus_cta_section',
    'container_inclusive' => true
));


/* ============CUSTOM SECTION 1============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_customa_section', array(
    'title' => esc_html__('Custom Section A', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_customa_section'),
    'hiding_control' => 'total_plus_customa_section_disable'
)));

$wp_customize->add_setting('total_plus_customa_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_customa_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_customa_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_customa_section_disable',
                'total_plus_customa_title_subtitle_heading',
                'total_plus_customa_super_title',
                'total_plus_customa_title',
                'total_plus_customa_sub_title',
                'total_plus_customa_title_style',
                'total_plus_customa_page_heading',
                'total_plus_customa_page'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_customa_enable_fullwindow',
                'total_plus_customa_align_item',
                'total_plus_customa_bg_type',
                'total_plus_customa_bg_color',
                'total_plus_customa_bg_gradient',
                'total_plus_customa_bg_image',
                'total_plus_customa_parallax_effect',
                'total_plus_customa_bg_video',
                'total_plus_customa_overlay_color',
                'total_plus_customa_cs_heading',
                'total_plus_customa_super_title_color',
                'total_plus_customa_title_color',
                'total_plus_customa_text_color',
                'total_plus_customa_link_color',
                'total_plus_customa_link_hov_color',
                'total_plus_customa_cs_seperator',
                'total_plus_customa_padding_top',
                'total_plus_customa_padding_bottom',
                'total_plus_customa_section_seperator',
                'total_plus_customa_seperator1',
                'total_plus_customa_top_seperator',
                'total_plus_customa_ts_color',
                'total_plus_customa_ts_height',
                'total_plus_customa_seperator2',
                'total_plus_customa_bottom_seperator',
                'total_plus_customa_bs_color',
                'total_plus_customa_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE CUSTOM SECTION
$wp_customize->add_setting('total_plus_customa_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_customa_section_disable', array(
    'section' => 'total_plus_customa_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_customa_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_customa_title_subtitle_heading', array(
    'section' => 'total_plus_customa_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_customa_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customa_super_title', array(
    'section' => 'total_plus_customa_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customa_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Custom A Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customa_title', array(
    'section' => 'total_plus_customa_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customa_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Custom A Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customa_sub_title', array(
    'section' => 'total_plus_customa_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customa_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customa_title_style', array(
    'section' => 'total_plus_customa_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

$wp_customize->add_setting('total_plus_customa_page_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_customa_page_heading', array(
    'section' => 'total_plus_customa_section',
    'label' => esc_html__('Page', 'total-plus')
)));

$wp_customize->add_setting('total_plus_customa_page', array(
    'sanitize_callback' => 'absint',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customa_page', array(
    'section' => 'total_plus_customa_section',
    'type' => 'dropdown-pages',
    'label' => esc_html__('Select a Page', 'total-plus'),
    'description' => esc_html__('Create a custom layout with the selected page using pagebuilder.', 'total-plus')
));


$wp_customize->selective_refresh->add_partial('total_plus_customa_title_style', array(
    'selector' => '.ht-customa-section',
    'render_callback' => 'total_plus_customa_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customa_super_title', array(
    'selector' => '.ht-customa-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customa_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customa_title', array(
    'selector' => '.ht-customa-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customa_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customa_sub_title', array(
    'selector' => '.ht-customa-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customa_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customa_page', array(
    'selector' => '.ht-customa-content',
    'render_callback' => 'total_plus_customa_content'
));

/* ============CUSTOM SECTION 2============ */
$wp_customize->add_section(new Total_Plus_Toggle_Section($wp_customize, 'total_plus_customb_section', array(
    'title' => esc_html__('Custom Section B', 'total-plus'),
    'panel' => 'total_plus_home_panel',
    'priority' => total_plus_get_section_position('total_plus_customb_section'),
    'hiding_control' => 'total_plus_customb_section_disable'
)));

$wp_customize->add_setting('total_plus_customb_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_customb_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_customb_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Basic Settings', 'total-plus'),
            'fields' => array(
                'total_plus_customb_section_disable',
                'total_plus_customb_title_subtitle_heading',
                'total_plus_customb_super_title',
                'total_plus_customb_title',
                'total_plus_customb_sub_title',
                'total_plus_customb_title_style',
                'total_plus_customb_page_heading',
                'total_plus_customb_page'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Design Settings', 'total-plus'),
            'fields' => array(
                'total_plus_customb_enable_fullwindow',
                'total_plus_customb_align_item',
                'total_plus_customb_bg_type',
                'total_plus_customb_bg_color',
                'total_plus_customb_bg_gradient',
                'total_plus_customb_bg_image',
                'total_plus_customb_parallax_effect',
                'total_plus_customb_bg_video',
                'total_plus_customb_overlay_color',
                'total_plus_customb_cs_heading',
                'total_plus_customb_super_title_color',
                'total_plus_customb_title_color',
                'total_plus_customb_text_color',
                'total_plus_customb_link_color',
                'total_plus_customb_link_hov_color',
                'total_plus_customb_cs_seperator',
                'total_plus_customb_padding_top',
                'total_plus_customb_padding_bottom',
                'total_plus_customb_section_seperator',
                'total_plus_customb_seperator1',
                'total_plus_customb_top_seperator',
                'total_plus_customb_ts_color',
                'total_plus_customb_ts_height',
                'total_plus_customb_seperator2',
                'total_plus_customb_bottom_seperator',
                'total_plus_customb_bs_color',
                'total_plus_customb_bs_height'
            ),
        ),
    ),
)));

//ENABLE/DISABLE CUSTOM SECTION
$wp_customize->add_setting('total_plus_customb_section_disable', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'off'
));

$wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, 'total_plus_customb_section_disable', array(
    'section' => 'total_plus_customb_section',
    'label' => esc_html__('Disable Section', 'total-plus'),
    'on_off_label' => array(
        'on' => esc_html__('Yes', 'total-plus'),
        'off' => esc_html__('No', 'total-plus')
    ),
    'class' => 'switch-section'
)));

$wp_customize->add_setting('total_plus_customb_title_subtitle_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_customb_title_subtitle_heading', array(
    'section' => 'total_plus_customb_section',
    'label' => esc_html__('Section Title & Sub Title', 'total-plus')
)));

$wp_customize->add_setting('total_plus_customb_super_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customb_super_title', array(
    'section' => 'total_plus_customb_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customb_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Custom B Section', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customb_title', array(
    'section' => 'total_plus_customb_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customb_sub_title', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => esc_html__('Custom B Section SubTitle', 'total-plus'),
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customb_sub_title', array(
    'section' => 'total_plus_customb_section',
    'type' => 'textarea',
    'label' => esc_html__('Sub Title', 'total-plus')
));

$wp_customize->add_setting('total_plus_customb_title_style', array(
    'default' => 'ht-section-title-top-center',
    'sanitize_callback' => 'total_plus_sanitize_choices',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customb_title_style', array(
    'section' => 'total_plus_customb_section',
    'type' => 'select',
    'label' => esc_html__('Title Style', 'total-plus'),
    'choices' => total_plus_tagline_style()
));

$wp_customize->add_setting('total_plus_customb_page_heading', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, 'total_plus_customb_page_heading', array(
    'section' => 'total_plus_customb_section',
    'label' => esc_html__('Page', 'total-plus')
)));

$wp_customize->add_setting('total_plus_customb_page', array(
    'sanitize_callback' => 'absint',
    'transport' => 'postMessage'
));

$wp_customize->add_control('total_plus_customb_page', array(
    'section' => 'total_plus_customb_section',
    'type' => 'dropdown-pages',
    'label' => esc_html__('Select a Page', 'total-plus'),
    'description' => esc_html__('Create a custom layout with the selected page using pagebuilder.', 'total-plus')
));


$wp_customize->selective_refresh->add_partial('total_plus_customb_title_style', array(
    'selector' => '.ht-customb-section',
    'render_callback' => 'total_plus_customb_section',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customb_super_title', array(
    'selector' => '.ht-customb-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customb_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customb_title', array(
    'selector' => '.ht-customb-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customb_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customb_sub_title', array(
    'selector' => '.ht-customb-section .ht-section-title-tagline',
    'render_callback' => 'total_plus_customb_title',
    'container_inclusive' => true
));

$wp_customize->selective_refresh->add_partial('total_plus_customb_page', array(
    'selector' => '.ht-customb-content',
    'render_callback' => 'total_plus_customb_content'
));


$section_array = total_plus_sections_array();
$exculde_section_array = array('about', 'contact', 'tab', 'cta', 'customa', 'customb');

foreach ($section_array as $id) {

    $wp_customize->add_setting("total_plus_{$id}_enable_fullwindow", array(
        'default' => 'off',
        'sanitize_callback' => 'total_plus_sanitize_text',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Switch_Control($wp_customize, "total_plus_{$id}_enable_fullwindow", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Full Window Section', 'total-plus'),
        'on_off_label' => array(
            'on' => esc_html__('Yes', 'total-plus'),
            'off' => esc_html__('No', 'total-plus')
        ),
        'priority' => 5
    )));

    $wp_customize->add_setting("total_plus_{$id}_align_item", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
        'default' => 'top',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_align_item", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'radio',
        'label' => esc_html__('Content Alignment', 'total-plus'),
        'choices' => array(
            'top' => esc_html__('Top', 'total-plus'),
            'middle' => esc_html__('Middle', 'total-plus'),
            'bottom' => esc_html__('Bottom', 'total-plus')
        ),
        'priority' => 10
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_type", array(
        'default' => 'color-bg',
        'sanitize_callback' => 'total_plus_sanitize_choices',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_bg_type", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Background Type', 'total-plus'),
        'choices' => array(
            'color-bg' => esc_html__('Color Background', 'total-plus'),
            'gradient-bg' => esc_html__('Gradient Background', 'total-plus'),
            'image-bg' => esc_html__('Image Background', 'total-plus'),
            'video-bg' => esc_html__('Video Background', 'total-plus')
        ),
        'priority' => 15
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_color", array(
        'default' => '#FFFFFF',
        'sanitize_callback' => 'sanitize_hex_color',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_bg_color", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Background Color', 'total-plus'),
        'priority' => 20
    )));

    $wp_customize->add_setting("total_plus_{$id}_bg_gradient", array(
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Gradient_Control($wp_customize, "total_plus_{$id}_bg_gradient", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Gradient Background', 'total-plus'),
        'priority' => 25
    )));

    // Registers example_background settings
    $wp_customize->add_setting("total_plus_{$id}_bg_image_url", array(
        'sanitize_callback' => 'esc_url_raw',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_image_id", array(
        'sanitize_callback' => 'absint',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_image_repeat", array(
        'default' => 'no-repeat',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_image_size", array(
        'default' => 'cover',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_position", array(
        'default' => 'center-center',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_image_attach", array(
        'default' => 'fixed',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));

    // Registers example_background control
    $wp_customize->add_control(new Total_Plus_Background_Control($wp_customize, "total_plus_{$id}_bg_image", array(
        'label' => esc_html__('Background Image', 'total-plus'),
        'section' => "total_plus_{$id}_section",
        'settings' => array(
            'image_url' => "total_plus_{$id}_bg_image_url",
            'image_id' => "total_plus_{$id}_bg_image_id",
            'repeat' => "total_plus_{$id}_bg_image_repeat", // Use false to hide the field
            'size' => "total_plus_{$id}_bg_image_size",
            'position' => "total_plus_{$id}_bg_position",
            'attach' => "total_plus_{$id}_bg_image_attach"
        ),
        'priority' => 30
    )));

    $wp_customize->add_setting("total_plus_{$id}_parallax_effect", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
        'default' => 'none',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_parallax_effect", array(
        'type' => 'radio',
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Background Effect', 'total-plus'),
        'choices' => array(
            'none' => esc_html__('None', 'total-plus'),
            'parallax' => esc_html__('Enable Parallax', 'total-plus'),
            'scroll' => esc_html__('Horizontal Moving', 'total-plus')
        ),
        'priority' => 35
    ));

    $wp_customize->add_setting("total_plus_{$id}_bg_video", array(
        'default' => '6O9Nd1RSZSY',
        'sanitize_callback' => 'total_plus_sanitize_text'
    ));

    $wp_customize->add_control("total_plus_{$id}_bg_video", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'text',
        'label' => esc_html__('Youtube Video ID', 'total-plus'),
        'description' => esc_html__('https://www.youtube.com/watch?v=yNAsk4Zw2p0. Add only yNAsk4Zw2p0', 'total-plus'),
        'priority' => 40
    ));

    $wp_customize->add_setting("total_plus_{$id}_overlay_color", array(
        'default' => 'rgba(255,255,255,0)',
        'sanitize_callback' => 'total_plus_sanitize_color_alpha',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Alpha_Color_Control($wp_customize, "total_plus_{$id}_overlay_color", array(
        'label' => esc_html__('Background Overlay Color', 'total-plus'),
        'section' => "total_plus_{$id}_section",
        'palette' => array(
            'rgb(255, 255, 255, 0.3)', // RGB, RGBa, and hex values supported
            'rgba(0, 0, 0, 0.3)',
            'rgba( 255, 255, 255, 0.2 )', // Different spacing = no problem
            '#00CC99', // Mix of color types = no problem
            '#00C439',
            '#00C569',
            'rgba( 255, 234, 255, 0.2 )', // Different spacing = no problem
            '#AACC99', // Mix of color types = no problem
            '#33C439',
        ),
        'priority' => 45
    )));

    if ($id != 'contact') {
        $wp_customize->add_setting("total_plus_{$id}_cs_heading", array(
            'sanitize_callback' => 'total_plus_sanitize_text'
        ));

        $wp_customize->add_control(new Total_Plus_Customize_Heading($wp_customize, "total_plus_{$id}_cs_heading", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Color Settings', 'total-plus'),
            'priority' => 50
        )));

        $wp_customize->add_setting("total_plus_{$id}_super_title_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_super_title_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Section Super Title Color', 'total-plus'),
            'priority' => 55
        )));

        $wp_customize->add_setting("total_plus_{$id}_title_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_title_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Section Title Color', 'total-plus'),
            'priority' => 55
        )));

        $wp_customize->add_setting("total_plus_{$id}_text_color", array(
            'default' => '#333333',
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_text_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Section Text Color', 'total-plus'),
            'priority' => 55
        )));

        $wp_customize->add_setting("total_plus_{$id}_link_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_link_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Section Link Color', 'total-plus'),
            'priority' => 55
        )));

        $wp_customize->add_setting("total_plus_{$id}_link_hov_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_link_hov_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('Section Link Hover Color', 'total-plus'),
            'priority' => 55
        )));
    }

    if (!in_array($id, $exculde_section_array)) {
        $wp_customize->add_setting("total_plus_{$id}_mb_bg_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_mb_bg_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('More Button Background Color', 'total-plus'),
            'priority' => 60
        )));

        $wp_customize->add_setting("total_plus_{$id}_mb_text_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_mb_text_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('More Button Text Color', 'total-plus'),
            'priority' => 65
        )));

        $wp_customize->add_setting("total_plus_{$id}_mb_hov_bg_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_mb_hov_bg_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('More Button Hover Background Color', 'total-plus'),
            'priority' => 70
        )));

        $wp_customize->add_setting("total_plus_{$id}_mb_hov_text_color", array(
            'sanitize_callback' => 'sanitize_hex_color',
            'transport' => 'postMessage'
        ));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_mb_hov_text_color", array(
            'section' => "total_plus_{$id}_section",
            'label' => esc_html__('More Button Hover Text Color', 'total-plus'),
            'priority' => 75
        )));
    }

    $wp_customize->add_setting("total_plus_{$id}_cs_seperator", array(
        'sanitize_callback' => 'total_plus_sanitize_text'
    ));

    $wp_customize->add_control(new Total_Plus_Separator_Control($wp_customize, "total_plus_{$id}_cs_seperator", array(
        'section' => "total_plus_{$id}_section",
        'priority' => 80
    )));

    $wp_customize->add_setting("total_plus_{$id}_padding_top", array(
        'sanitize_callback' => 'absint',
        'default' => 100,
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, "total_plus_{$id}_padding_top", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Padding Top (px)', 'total-plus'),
        'options' => array(
            'min' => 0,
            'max' => 400,
            'step' => 5,
        ),
        'priority' => 85
    )));

    $wp_customize->add_setting("total_plus_{$id}_padding_bottom", array(
        'sanitize_callback' => 'absint',
        'default' => 100,
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, "total_plus_{$id}_padding_bottom", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Padding Bottom (px)', 'total-plus'),
        'options' => array(
            'min' => 0,
            'max' => 400,
            'step' => 5,
        ),
        'priority' => 90
    )));

    $wp_customize->add_setting("total_plus_{$id}_section_seperator", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
        'default' => 'no',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_section_seperator", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Enable Seperator', 'total-plus'),
        'choices' => array(
            'no' => esc_html__('Disable', 'total-plus'),
            'top' => esc_html__('Enable Top Seperator', 'total-plus'),
            'bottom' => esc_html__('Enable Bottom Seperator', 'total-plus'),
            'top-bottom' => esc_html__('Enable Top & Bottom Seperator', 'total-plus')
        ),
        'priority' => 95
    ));

    $wp_customize->add_setting("total_plus_{$id}_seperator1", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
    ));

    $wp_customize->add_control(new Total_Plus_Separator_Control($wp_customize, "total_plus_{$id}_seperator1", array(
        'section' => "total_plus_{$id}_section",
        'priority' => 100
    )));

    $wp_customize->add_setting("total_plus_{$id}_top_seperator", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
        'default' => 'big-triangle-center',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_top_seperator", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Top Seperator', 'total-plus'),
        'choices' => total_plus_svg_seperator(),
        'priority' => 105
    ));

    $wp_customize->add_setting("total_plus_{$id}_ts_color", array(
        'default' => '#FF0000',
        'sanitize_callback' => 'sanitize_hex_color',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_ts_color", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Top Seperator Color', 'total-plus'),
        'priority' => 115
    )));

    $wp_customize->add_setting("total_plus_{$id}_ts_height", array(
        'sanitize_callback' => 'absint',
        'default' => 60,
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, "total_plus_{$id}_ts_height", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Top Seperator Height (px)', 'total-plus'),
        'options' => array(
            'min' => 20,
            'max' => 200,
            'step' => 1,
        ),
        'priority' => 120
    )));

    $wp_customize->add_setting("total_plus_{$id}_seperator2", array(
        'sanitize_callback' => 'total_plus_sanitize_text'
    ));

    $wp_customize->add_control(new Total_Plus_Separator_Control($wp_customize, "total_plus_{$id}_seperator2", array(
        'section' => "total_plus_{$id}_section",
        'priority' => 125
    )));

    $wp_customize->add_setting("total_plus_{$id}_bottom_seperator", array(
        'sanitize_callback' => 'total_plus_sanitize_text',
        'default' => 'big-triangle-center',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control("total_plus_{$id}_bottom_seperator", array(
        'section' => "total_plus_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Bottom Seperator', 'total-plus'),
        'choices' => total_plus_svg_seperator(),
        'priority' => 130
    ));

    $wp_customize->add_setting("total_plus_{$id}_bs_color", array(
        'default' => '#FF0000',
        'sanitize_callback' => 'sanitize_hex_color',
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, "total_plus_{$id}_bs_color", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Bottom Seperator Color', 'total-plus'),
        'priority' => 135,
    )));

    $wp_customize->add_setting("total_plus_{$id}_bs_height", array(
        'sanitize_callback' => 'absint',
        'default' => 60,
        'transport' => 'postMessage'
    ));

    $wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, "total_plus_{$id}_bs_height", array(
        'section' => "total_plus_{$id}_section",
        'label' => esc_html__('Bottom Seperator Height (px)', 'total-plus'),
        'options' => array(
            'min' => 20,
            'max' => 200,
            'step' => 1,
        ),
        'priority' => 140
    )));

    $wp_customize->selective_refresh->add_partial("total_plus_{$id}_bg_type", array(
        'selector' => "#ht-{$id}-section",
        'render_callback' => "total_plus_{$id}_section",
        'container_inclusive' => true
    ));

    $wp_customize->selective_refresh->add_partial("total_plus_{$id}_parallax_effect", array(
        'selector' => "#ht-{$id}-section",
        'render_callback' => "total_plus_{$id}_section",
        'container_inclusive' => true
    ));

    $wp_customize->selective_refresh->add_partial("total_plus_{$id}_section_seperator", array(
        'selector' => "#ht-{$id}-section",
        'render_callback' => "total_plus_{$id}_section",
        'container_inclusive' => true
    ));

    $wp_customize->selective_refresh->add_partial("total_plus_{$id}_top_seperator", array(
        'selector' => "#ht-{$id}-section",
        'render_callback' => "total_plus_{$id}_section",
        'container_inclusive' => true
    ));

    $wp_customize->selective_refresh->add_partial("total_plus_{$id}_bottom_seperator", array(
        'selector' => "#ht-{$id}-section",
        'render_callback' => "total_plus_{$id}_section",
        'container_inclusive' => true
    ));
}
