jQuery(document).ready(function ($) {

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    jQuery('html').addClass('colorpicker-ready');

    $('body').on('click', '#customize-control-total_plus_maintenance_social a, #customize-control-total_plus_social_link a, #customize-control-total_plus_contact_social_link a', function () {
        wp.customize.section('total_plus_social_section').expand();
        return false;
    });

    //Homepage Section Sortable
    function total_plus_sections_order(container) {
        var sections = $(container).sortable('toArray');
        var s_ordered = [];
        $.each(sections, function (index, s_id) {
            s_id = s_id.replace("accordion-section-", "");
            s_ordered.push(s_id);
        });

        $.ajax({
            url: ajaxurl,
            type: 'post',
            dataType: 'html',
            data: {
                'action': 'total_plus_order_sections',
                'sections': s_ordered,
            }
        }).done(function (data) {
            $.each(s_ordered, function (key, value) {
                wp.customize.section(value).priority(key);
            });
            $(container).find( '.total-drag-spinner' ).hide();
            wp.customize.previewer.refresh();
        });
    }

    $('#sub-accordion-panel-total_plus_home_panel').sortable({
        axis: 'y',
        helper: 'clone',
        cursor: 'move',
        items: '> li.control-section:not(#accordion-section-total_plus_slider_section)',
        delay: 150,
        update: function (event, ui) {
            $('#sub-accordion-panel-total_plus_home_panel').find( '.total-drag-spinner' ).show();
            total_plus_sections_order('#sub-accordion-panel-total_plus_home_panel');
            $( '.wp-full-overlay-sidebar-content' ).scrollTop( 0 );
        }
    });


    //Homepage section - control visiblity toggle
    var settingIds = ['about', 'highlight', 'featured', 'portfolio', 'service', 'team', 'counter', 'testimonial', 'blog', 'logo', 'cta', 'pricing', 'news', 'tab', 'contact', 'customa', 'customb'];

    $.each(settingIds, function (i, settingId) {
        wp.customize('total_plus_' + settingId + '_bg_type', function (setting) {
            var setupControlColorBg = function (control) {
                var visibility = function () {
                    if ('color-bg' === setting.get() || 'image-bg' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            var setupControlImageBg = function (control) {
                var visibility = function () {
                    if ('image-bg' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            var setupControlVideoBg = function (control) {
                var visibility = function () {
                    if ('video-bg' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            var setupControlGradientBg = function (control) {
                var visibility = function () {
                    if ('gradient-bg' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            var setupControlOverlay = function (control) {
                var visibility = function () {
                    if ('color-bg' === setting.get() || 'gradient-bg' === setting.get()) {
                        control.container.addClass('customizer-hidden');
                    } else {
                        control.container.removeClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            wp.customize.control('total_plus_' + settingId + '_bg_color', setupControlColorBg);
            wp.customize.control('total_plus_' + settingId + '_bg_image', setupControlImageBg);
            wp.customize.control('total_plus_' + settingId + '_parallax_effect', setupControlImageBg);
            wp.customize.control('total_plus_' + settingId + '_bg_video', setupControlVideoBg);
            wp.customize.control('total_plus_' + settingId + '_bg_gradient', setupControlGradientBg);
            wp.customize.control('total_plus_' + settingId + '_overlay_color', setupControlOverlay);
        });
    });

    $.each(settingIds, function (i, settingId) {
        wp.customize('total_plus_' + settingId + '_section_seperator', function (setting) {

            var setupTopSeperator = function (control) {
                var visibility = function () {
                    if ('top' === setting.get() || 'top-bottom' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            var setupBottomSeperator = function (control) {
                var visibility = function () {
                    if ('bottom' === setting.get() || 'top-bottom' === setting.get()) {
                        control.container.removeClass('customizer-hidden');
                    } else {
                        control.container.addClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            wp.customize.control('total_plus_' + settingId + '_seperator1', setupTopSeperator);
            wp.customize.control('total_plus_' + settingId + '_top_seperator', setupTopSeperator);
            wp.customize.control('total_plus_' + settingId + '_ts_color', setupTopSeperator);
            wp.customize.control('total_plus_' + settingId + '_ts_height', setupTopSeperator);
            wp.customize.control('total_plus_' + settingId + '_seperator2', setupBottomSeperator);
            wp.customize.control('total_plus_' + settingId + '_bottom_seperator', setupBottomSeperator);
            wp.customize.control('total_plus_' + settingId + '_bs_color', setupBottomSeperator);
            wp.customize.control('total_plus_' + settingId + '_bs_height', setupBottomSeperator);
        });
    });

    $.each(settingIds, function (i, settingId) {
        wp.customize('total_plus_' + settingId + '_enable_fullwindow', function (setting) {

            var setupControlFullWindow = function (control) {
                var visibility = function () {
                    if ('off' === setting.get()) {
                        control.container.addClass('customizer-hidden');
                    } else {
                        control.container.removeClass('customizer-hidden');
                    }
                };
                visibility();
                setting.bind(visibility);
            };

            wp.customize.control('total_plus_' + settingId + '_align_item', setupControlFullWindow);
        });
    });

    wp.customize('total_plus_preloader_type', function (setting) {
        var setupControl = function (control) {
            var visibility = function () {
                if ('custom' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        var setupControl1 = function (control) {
            var visibility = function () {
                if ('custom' !== setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        wp.customize.control('total_plus_preloader_image', setupControl);
        wp.customize.control('total_plus_preloader_color', setupControl1);
    });

    wp.customize('total_plus_archive_content', function (setting) {
        var setupControl = function (control) {
            var visibility = function () {
                if ('excerpt' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        wp.customize.control('total_plus_archive_excerpt_length', setupControl);
        wp.customize.control('total_plus_archive_readmore', setupControl);
    });

    wp.customize('total_plus_mh_layout', function (setting) {
        var setupControl = function (control) {
            var visibility = function () {
                if ('header-style1' === setting.get() || 'header-style4' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        wp.customize.control('total_plus_header_position', setupControl);
    });

    wp.customize('total_plus_maintenance_bg_type', function (setting) {
        var setupControlSlider = function (control) {
            var visibility = function () {
                if ('slider' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        var setupControlShortcode = function (control) {
            var visibility = function () {
                if ('revolution' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        var setupControlBanner = function (control) {
            var visibility = function () {
                if ('banner' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        var setupControlVideo = function (control) {
            var visibility = function () {
                if ('video' === setting.get()) {
                    control.container.removeClass('customizer-hidden');
                } else {
                    control.container.addClass('customizer-hidden');
                }
            };
            visibility();
            setting.bind(visibility);
        };

        wp.customize.control('total_plus_maintenance_banner_image', setupControlBanner);
        wp.customize.control('total_plus_maintenance_slider_shortcode', setupControlShortcode);
        wp.customize.control('total_plus_maintenance_sliders', setupControlSlider);
        wp.customize.control('total_plus_maintenance_slider_info', setupControlSlider);
        wp.customize.control('total_plus_maintenance_slider_pause', setupControlSlider);
        wp.customize.control('total_plus_maintenance_video', setupControlVideo);
    });

    //FontAwesome Icon Control JS
    $('body').on('click', '.total-plus-customizer-icon-box .total-plus-icon-list li', function () {
        var icon_class = $(this).find('i').attr('class');
        $(this).closest('.total-plus-icon-box').find('.total-plus-icon-list li').removeClass('icon-active');
        $(this).addClass('icon-active');
        $(this).closest('.total-plus-icon-box').prev('.total-plus-selected-icon').children('i').attr('class', '').addClass(icon_class);
        $(this).closest('.total-plus-icon-box').next('input').val(icon_class).trigger('change');
        $(this).closest('.total-plus-icon-box').slideUp();
    });

    $('body').on('click', '.total-plus-customizer-icon-box .total-plus-selected-icon', function () {
        $(this).next().slideToggle();
    });

    $('body').on('change', '.total-plus-customizer-icon-box .total-plus-icon-search select', function () {
        var selected = $(this).val();
        $(this).closest('.total-plus-icon-box').find('.total-plus-icon-search-input').val('');
        $(this).closest('.total-plus-icon-box').find('.total-plus-icon-list li').show();
        $(this).closest('.total-plus-icon-box').find('.total-plus-icon-list').hide().removeClass('active');
        $(this).closest('.total-plus-icon-box').find('.' + selected).fadeIn().addClass('active');
    });

    $('body').on('keyup', '.total-plus-customizer-icon-box .total-plus-icon-search input', function (e) {
        var $input = $(this);
        var keyword = $input.val().toLowerCase();
        search_criteria = $input.closest('.total-plus-icon-box').find('.total-plus-icon-list.active i');

        delay(function () {
            $(search_criteria).each(function () {
                if ($(this).attr('class').indexOf(keyword) > -1) {
                    $(this).parent().show();
                } else {
                    $(this).parent().hide();
                }
            });
        }, 500);
    });

    //Switch Control
    $('body').on('click', '.onoffswitch', function () {
        var $this = $(this);
        if ($this.hasClass('switch-on')) {
            $(this).removeClass('switch-on');
            $this.next('input').val('off').trigger('change')
        } else {
            $(this).addClass('switch-on');
            $this.next('input').val('on').trigger('change')
        }
    });


    // Gallery Control
    $('.upload_gallery_button').click(function (event) {
        var current_gallery = $(this).closest('label');

        if (event.currentTarget.id === 'clear-gallery') {
            //remove value from input
            current_gallery.find('.gallery_values').val('').trigger('change');

            //remove preview images
            current_gallery.find('.gallery-screenshot').html('');
            return;
        }

        // Make sure the media gallery API exists
        if (typeof wp === 'undefined' || !wp.media || !wp.media.gallery) {
            return;
        }
        event.preventDefault();

        // Activate the media editor
        var val = current_gallery.find('.gallery_values').val();
        var final;

        if (!val) {
            final = '[gallery ids="0"]';
        } else {
            final = '[gallery ids="' + val + '"]';
        }
        var frame = wp.media.gallery.edit(final);

        frame.state('gallery-edit').on(
                'update',
                function (selection) {

                    //clear screenshot div so we can append new selected images
                    current_gallery.find('.gallery-screenshot').html('');

                    var element, preview_html = '',
                            preview_img;
                    var ids = selection.models.map(
                            function (e) {
                                element = e.toJSON();
                                preview_img = typeof element.sizes.thumbnail !== 'undefined' ? element.sizes.thumbnail.url : element.url;
                                preview_html = "<div class='screen-thumb'><img src='" + preview_img + "'/></div>";
                                current_gallery.find('.gallery-screenshot').append(preview_html);
                                return e.id;
                            }
                    );

                    current_gallery.find('.gallery_values').val(ids.join(',')).trigger('change');
                }
        );
        return false;
    });

    //MultiCheck box Control JS
    $('.customize-control-checkbox-multiple input[type="checkbox"]').on('change', function () {

        var checkbox_values = $(this).parents('.customize-control').find('input[type="checkbox"]:checked').map(
                function () {
                    return $(this).val();
                }
        ).get().join(',');

        $(this).parents('.customize-control').find('input[type="hidden"]').val(checkbox_values).trigger('change');

    });

    //Chosen JS
    $(".hs-chosen-select, .customize-control-typography select").chosen({
        width: "100%"
    });

    //Image Selector JS
    $('body').on('click', '.selector-labels label', function () {
        var $this = $(this);
        var value = $this.attr('data-val');
        $this.siblings().removeClass('selector-selected');
        $this.addClass('selector-selected');
        $this.closest('.selector-labels').next('input').val(value).trigger('change');
    });

    $('body').on('change', '.total-plus-type-radio input[type="radio"]', function () {
        var $this = $(this);
        $this.parent('label').siblings('label').find('input[type="radio"]').prop('checked', false);
        var value = $this.closest('.radio-labels').find('input[type="radio"]:checked').val();
        $this.closest('.radio-labels').next('input').val(value).trigger('change');
    });

    //Range Slider JS
    $('.customize-control-custom_range .range-input').each(function () {
        var $this = $(this);
        $this.slider({
            range: "min",
            value: parseFloat($this.attr('data-value')),
            min: parseFloat($this.attr('data-min')),
            max: parseFloat($this.attr('data-max')),
            step: parseFloat($this.attr('data-step')),
            slide: function (event, ui) {
                $this.next(".range-input-selector").text(ui.value);

                var setting = $(this).next(".range-input-selector").attr('data-customize-setting-link');

                // Set the new value.
                wp.customize(setting, function (obj) {
                    obj.set(ui.value);
                });
            }
        });
    });

    //TinyMCE editor
    $(document).on('tinymce-editor-init', function () {
        $('.customize-control').find('.wp-editor-area').each(function () {
            var tArea = $(this),
                    id = tArea.attr('id'),
                    input = $('input[data-customize-setting-link="' + id + '"]'),
                    editor = tinyMCE.get(id),
                    content;

            if (editor) {
                editor.onChange.add(function () {
                    this.save();
                    content = editor.getContent();
                    input.val(content).trigger('change');
                });
            }

            tArea.css({
                visibility: 'visible'
            }).on('keyup', function () {
                content = tArea.val();
                input.val(content).trigger('change');
            });

        });
    });

    //Select Image Js
    $('.select-image-control').on('change', function () {
        var activeImage = $(this).find(':selected').attr('data-image');
        $(this).next('.select-image-wrap').find('img').attr('src', activeImage);
    });

    //Date Picker Js
    $(".ht-datepicker-control").datepicker({
        dateFormat: "yy/mm/dd"
    });

    //Scroll to section
    $('body').on('click', '#sub-accordion-panel-total_plus_home_panel .control-subsection .accordion-section-title', function (event) {
        var section_id = $(this).parent('.control-subsection').attr('id');
        scrollToSection(section_id);
    });

    //Scroll to Footer
    $('body').on('click', '#accordion-section-total_plus_footer_section .accordion-section-title', function (event) {
        var preview_section_id = "ht-colophon";
        var $contents = jQuery('#customize-preview iframe').contents();

        if ($contents.find('#' + preview_section_id).length > 0) {
            $contents.find("html, body").animate({
                scrollTop: $contents.find("#" + preview_section_id).offset().top
            }, 1000);
        }
    });

    function total_plus_refresh_repeater_values() {
        $(".total-plus-repeater-field-control-wrap").each(function () {

            var values = [];
            var $this = $(this);

            $this.find(".total-plus-repeater-field-control").each(function () {
                var valueToPush = {};

                $(this).find('[data-name]').each(function () {
                    var dataName = $(this).attr('data-name');
                    var dataValue = $(this).val();
                    valueToPush[dataName] = dataValue;
                });

                values.push(valueToPush);
            });

            $this.next('.total-plus-repeater-collector').val(JSON.stringify(values)).trigger('change');
        });
    }

    $('#customize-theme-controls').on('click', '.total-plus-repeater-field-title', function () {
        $(this).next().slideToggle();
        $(this).closest('.total-plus-repeater-field-control').toggleClass('expanded');
    });

    $('#customize-theme-controls').on('click', '.total-plus-repeater-field-close', function () {
        $(this).closest('.total-plus-repeater-fields').slideUp();
        $(this).closest('.total-plus-repeater-field-control').toggleClass('expanded');
    });

    $("body").on("click", '.total-plus-add-control-field', function () {

        var $this = $(this).parent();
        if (typeof $this != 'undefined') {

            var field = $this.find(".total-plus-repeater-field-control:first").clone();
            if (typeof field != 'undefined') {

                field.find("input[type='text'][data-name]").each(function () {
                    var defaultValue = $(this).attr('data-default');
                    $(this).val(defaultValue);
                });

                field.find("textarea[data-name]").each(function () {
                    var defaultValue = $(this).attr('data-default');
                    $(this).val(defaultValue);
                });

                field.find("select[data-name]").each(function () {
                    var defaultValue = $(this).attr('data-default');
                    $(this).val(defaultValue);
                });

                field.find(".radio-labels input[type='radio']").each(function () {
                    var defaultValue = $(this).closest('.radio-labels').next('input[data-name]').attr('data-default');
                    $(this).closest('.radio-labels').next('input[data-name]').val(defaultValue);

                    if ($(this).val() == defaultValue) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                });

                field.find(".selector-labels label").each(function () {
                    var defaultValue = $(this).closest('.selector-labels').next('input[data-name]').attr('data-default');
                    var dataVal = $(this).attr('data-val');
                    $(this).closest('.selector-labels').next('input[data-name]').val(defaultValue);

                    if (defaultValue == dataVal) {
                        $(this).addClass('selector-selected');
                    } else {
                        $(this).removeClass('selector-selected');
                    }
                });

                field.find('.range-input').each(function () {
                    var $dis = $(this);
                    $dis.removeClass('ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all').empty();
                    var defaultValue = parseFloat($dis.attr('data-defaultvalue'));
                    $dis.siblings(".range-input-selector").val(defaultValue);
                    $dis.slider({
                        range: "min",
                        value: parseFloat($dis.attr('data-defaultvalue')),
                        min: parseFloat($dis.attr('data-min')),
                        max: parseFloat($dis.attr('data-max')),
                        step: parseFloat($dis.attr('data-step')),
                        slide: function (event, ui) {
                            $dis.siblings(".range-input-selector").val(ui.value);
                            total_plus_refresh_repeater_values();
                        }
                    });
                });

                field.find('.onoffswitch').each(function () {
                    var defaultValue = $(this).next('input[data-name]').attr('data-default');
                    $(this).next('input[data-name]').val(defaultValue);
                    if (defaultValue == 'on') {
                        $(this).addClass('switch-on');
                    } else {
                        $(this).removeClass('switch-on');
                    }
                });
                
                field.find('.total-plus-color-picker').each(function () {
                    $colorPicker = $(this);
                    $colorPicker.closest('.wp-picker-container').after($(this));
                    $colorPicker.prev('.wp-picker-container').remove();
                    $(this).wpColorPicker({
                        change: function (event, ui) {
                            setTimeout(function () {
                                total_plus_refresh_repeater_values();
                            }, 100);
                        }
                    });
                });

                field.find(".attachment-media-view").each(function () {
                    var defaultValue = $(this).find('input[data-name]').attr('data-default');
                    $(this).find('input[data-name]').val(defaultValue);
                    if (defaultValue) {
                        $(this).find(".thumbnail-image").html('<img src="' + defaultValue + '"/>').prev('.placeholder').addClass('hidden');
                    } else {
                        $(this).find(".thumbnail-image").html('').prev('.placeholder').removeClass('hidden');
                    }
                });

                field.find(".total-plus-icon-list").each(function () {
                    var defaultValue = $(this).next('input[data-name]').attr('data-default');
                    $(this).next('input[data-name]').val(defaultValue);
                    $(this).prev('.total-plus-selected-icon').children('i').attr('class', '').addClass(defaultValue);

                    $(this).find('li').each(function () {
                        var icon_class = $(this).find('i').attr('class');
                        if (defaultValue == icon_class) {
                            $(this).addClass('icon-active');
                        } else {
                            $(this).removeClass('icon-active');
                        }
                    });
                });

                field.find(".total-plus-multi-category-list").each(function () {
                    var defaultValue = $(this).next('input[data-name]').attr('data-default');
                    $(this).next('input[data-name]').val(defaultValue);

                    $(this).find('input[type="checkbox"]').each(function () {
                        if ($(this).val() == defaultValue) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }
                    });
                });

                field.find('.total-plus-fields').show();

                $this.find('.total-plus-repeater-field-control-wrap').append(field);

                field.addClass('expanded').find('.total-plus-repeater-fields').show();
                $('.accordion-section-content').animate({
                    scrollTop: $this.height()
                }, 1000);
                total_plus_refresh_repeater_values();
            }

        }
        return false;
    });

    $("#customize-theme-controls").on("click", ".total-plus-repeater-field-remove", function () {
        if (typeof $(this).parent() != 'undefined') {
            $(this).closest('.total-plus-repeater-field-control').slideUp('normal', function () {
                $(this).remove();
                total_plus_refresh_repeater_values();
            });
        }
        return false;
    });

    $("#customize-theme-controls").on('keyup change', '[data-name]', function () {
        delay(function () {
            total_plus_refresh_repeater_values();
            return false;
        }, 500);
    });

    $("#customize-theme-controls").on('change', 'input[type="checkbox"][data-name]', function () {
        if ($(this).is(":checked")) {
            $(this).val('yes');
        } else {
            $(this).val('no');
        }
        total_plus_refresh_repeater_values();
        return false;
    });

    /*Drag and drop to change order*/
    $(".total-plus-repeater-field-control-wrap").sortable({
        orientation: "vertical",
        handle: ".total-plus-repeater-field-title",
        update: function (event, ui) {
            total_plus_refresh_repeater_values();
        }
    });

    // Set all variables to be used in scope
    var frame;

    // ADD IMAGE LINK
    $('.customize-control-repeater').on('click', '.total-plus-upload-button', function (event) {
        event.preventDefault();

        var imgContainer = $(this).closest('.total-plus-fields-wrap').find('.thumbnail-image'),
                placeholder = $(this).closest('.total-plus-fields-wrap').find('.placeholder'),
                imgIdInput = $(this).siblings('.upload-id');

        // Create a new media frame
        frame = wp.media({
            title: 'Select or Upload Image',
            button: {
                text: 'Use Image'
            },
            multiple: false // Set to true to allow multiple files to be selected
        });

        // When an image is selected in the media frame...
        frame.on('select', function () {

            // Get media attachment details from the frame state
            var attachment = frame.state().get('selection').first().toJSON();

            // Send the attachment URL to our custom image input field.
            imgContainer.html('<img src="' + attachment.url + '" style="max-width:100%;"/>');
            placeholder.addClass('hidden');

            // Send the attachment id to our hidden input
            imgIdInput.val(attachment.url).trigger('change');

        });

        // Finally, open the modal on click
        frame.open();
    });


    // DELETE IMAGE LINK
    $('.customize-control-repeater').on('click', '.total-plus-delete-button', function (event) {

        event.preventDefault();
        var imgContainer = $(this).closest('.total-plus-fields-wrap').find('.thumbnail-image'),
                placeholder = $(this).closest('.total-plus-fields-wrap').find('.placeholder'),
                imgIdInput = $(this).siblings('.upload-id');

        // Clear out the preview image
        imgContainer.find('img').remove();
        placeholder.removeClass('hidden');

        // Delete the image id from the hidden input
        imgIdInput.val('').trigger('change');

    });

    $('.total-plus-color-picker').wpColorPicker({
        change: function (event, ui) {
            setTimeout(function () {
                total_plus_refresh_repeater_values()
            }, 100);
        }
    });

    //MultiCheck box Control JS
    $('body').on('change', '.total-plus-type-multicategory input[type="checkbox"]', function () {
        var checkbox_values = $(this).parents('.total-plus-type-multicategory').find('input[type="checkbox"]:checked').map(function () {
            return $(this).val();
        }).get().join(',');

        $(this).parents('.total-plus-type-multicategory').find('input[type="hidden"]').val(checkbox_values).trigger('change');
        total_plus_refresh_repeater_values();
    });

    $('.total-plus-repeater-fields .range-input').each(function () {
        var $dis = $(this);
        $dis.slider({
            range: "min",
            value: parseFloat($dis.attr('data-value')),
            min: parseFloat($dis.attr('data-min')),
            max: parseFloat($dis.attr('data-max')),
            step: parseFloat($dis.attr('data-step')),
            slide: function (event, ui) {
                $dis.siblings(".range-input-selector").val(ui.value);
                total_plus_refresh_repeater_values();
            }
        });
    });

});

function scrollToSection(section_id) {
    var preview_section_id = "ht-home-slider-section";

    var $contents = jQuery('#customize-preview iframe').contents();

    switch (section_id) {
        case 'accordion-section-total_plus_slider_section':
            preview_section_id = "ht-home-slider-section";
            break;

        case 'accordion-section-total_plus_about_section':
            preview_section_id = "ht-about-section";
            break;

        case 'accordion-section-total_plus_highlight_section':
            preview_section_id = "ht-highlight-section";
            break;

        case 'accordion-section-total_plus_featured_section':
            preview_section_id = "ht-featured-section";
            break;

        case 'accordion-section-total_plus_portfolio_section':
            preview_section_id = "ht-portfolio-section";
            break;

        case 'accordion-section-total_plus_service_section':
            preview_section_id = "ht-service-section";
            break;

        case 'accordion-section-total_plus_team_section':
            preview_section_id = "ht-team-section";
            break;

        case 'accordion-section-total_plus_pricing_section':
            preview_section_id = "ht-pricing-section";
            break;

        case 'accordion-section-total_plus_tab_section':
            preview_section_id = "ht-tab-section";
            break;

        case 'accordion-section-total_plus_news_section':
            preview_section_id = "ht-news-section";
            break;

        case 'accordion-section-total_plus_testimonial_section':
            preview_section_id = "ht-testimonial-section";
            break;

        case 'accordion-section-total_plus_counter_section':
            preview_section_id = "ht-counter-section";
            break;

        case 'accordion-section-total_plus_blog_section':
            preview_section_id = "ht-blog-section";
            break;

        case 'accordion-section-total_plus_logo_section':
            preview_section_id = "ht-logo-section";
            break;

        case 'accordion-section-total_plus_cta_section':
            preview_section_id = "ht-cta-section";
            break;

        case 'accordion-section-total_plus_contact_section':
            preview_section_id = "ht-contact-section";
            break;

        case 'accordion-section-total_plus_customa_section':
            preview_section_id = "ht-customa-section";
            break;

        case 'accordion-section-total_plus_customb_section':
            preview_section_id = "ht-customb-section";
            break;

    }

    if ($contents.find('#' + preview_section_id).length > 0) {
        $contents.find("html, body").animate({
            scrollTop: $contents.find("#" + preview_section_id).offset().top
        }, 1000);
    }

}

(function (api) {

    /**
     * Class extends the UploadControl
     */
    api.controlConstructor['background-image'] = api.UploadControl.extend({

        ready: function () {

            // Re-use ready function from parent class to set up the image uploader
            var image_url = this;
            image_url.setting = this.settings.image_url;
            api.UploadControl.prototype.ready.apply(image_url, arguments);

            // Set up the new controls
            var control = this;

            control.container.addClass('customize-control-image');

            control.container.on('click keydown', '.remove-button',
                    function () {
                        control.container.find('.background-image-fields').hide();
                    }
            );

            control.container.on('change', '.background-image-repeat select',
                    function () {
                        control.settings['repeat'].set(jQuery(this).val());
                    }
            );

            control.container.on('change', '.background-image-size select',
                    function () {
                        control.settings['size'].set(jQuery(this).val());
                    }
            );

            control.container.on('change', '.background-image-attach select',
                    function () {
                        control.settings['attach'].set(jQuery(this).val());
                    }
            );

            control.container.on('change', '.background-image-position select',
                    function () {
                        control.settings['position'].set(jQuery(this).val());
                    }
            );

        },

        /**
         * Callback handler for when an attachment is selected in the media modal.
         * Gets the selected image information, and sets it within the control.
         */
        select: function () {

            var attachment = this.frame.state().get('selection').first().toJSON();
            this.params.attachment = attachment;
            this.settings['image_url'].set(attachment.url);
            this.settings['image_id'].set(attachment.id);

        },

    });

})(wp.customize);

(function (wp, $) {
    if (!wp || !wp.customize) {
        return;
    }

    var api = wp.customize;
    api.TotalTabs = [];

    api.TotalTab = api.Control.extend({

        ready: function () {
            var control = this;
            control.container.find('a.customizer-tab').click(function (evt) {
                var tab = $(this).data('tab');
                evt.preventDefault();
                control.container.find('a.customizer-tab').removeClass('active');
                $(this).addClass('active');
                control.toggleActiveControls(tab);
            });

            api.TotalTabs.push(control.id);
        },

        toggleActiveControls: function (tab) {
            var control = this,
                    currentFields = control.params.buttons[tab].fields;
            _.each(control.params.fields, function (id) {
                var tabControl = api.control(id);
                if (undefined !== tabControl) {
                    if (tabControl.active() && $.inArray(id, currentFields) >= 0) {
                        tabControl.toggle(true);
                    } else {
                        tabControl.toggle(false);
                    }
                }
            });
        }

    });

    $.extend(api.controlConstructor, {
        'tab': api.TotalTab,
    });

    api.bind('ready', function () {
        _.each(api.TotalTabs, function (id) {
            var control = api.control(id);
            control.toggleActiveControls(0);
        });
    });

    api.controlConstructor['alpha-color'] = api.Control.extend({

        ready: function () {

            var control = this;

            var paletteInput = control.container.find('.alpha-color-control').data('palette');

            if (true == paletteInput) {
                palette = true;
            } else if (paletteInput.indexOf('|') !== -1) {
                palette = paletteInput.split('|');
            } else {
                palette = false;
            }

            control.container.find('.alpha-color-control').wpColorPicker({
                change: function (event, ui) {
                    var element = event.target;
                    var color = ui.color.toString();

                    if (jQuery('html').hasClass('colorpicker-ready')) {
                        control.setting.set(color);
                    }
                },
                clear: function (event) {
                    var element = jQuery(event.target).closest('.wp-picker-input-wrap').find('.wp-color-picker')[0];
                    var color = '';

                    if (element) {
                        control.setting.set(color);
                    }
                },
                palettes: palette
            });
        }
    });

})(window.wp, jQuery);

(function ($) {
    wp.customize.bind('ready', function () {
        wp.customize.section('total_plus_gdpr_section', function (section) {

            section.expanded.bind(function (isExpanding) {

                // Value of isExpanding will = true if you're entering the section, false if you're leaving it.
                if (isExpanding) {
                    wp.customize.previewer.send('total-plus-gdpr-add-class', {
                        expanded: isExpanding
                    });
                } else {
                    wp.customize.previewer.send('total-plus-gdpr-remove-class', {
                        home_url: wp.customize.settings.url.home
                    });
                }
            });

        });
    });
})(jQuery);
